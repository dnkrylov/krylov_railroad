package com.dkrylov.railroad.securitytest;

import com.dkrylov.railroad.configuration.HibernateConfig;
import com.dkrylov.railroad.configuration.MyBeanConfig;
import com.dkrylov.railroad.configuration.WebSecurityConfig;
import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.domain.UserRole;
import com.dkrylov.railroad.repositories.UserRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;

import java.time.LocalDate;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebSecurityConfig.class, MyBeanConfig.class, HibernateConfig.class})
@WebAppConfiguration
public class SecurityTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private UserRepo userRepo;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .build();
    }

    @Transactional
    @Test
    @Rollback
    public void vaildAuthentification() throws Exception {
        User user = new User("testUserFirstName", "testUserLastName",
                LocalDate.of(2000, 01, 01), "testUserLogin",
                new BCryptPasswordEncoder().encode("testUserPassword"), UserRole.ROLE_ADMIN);
        userRepo.addUser(user);

        mvc.perform(formLogin("/unregistered/tryToLogin").user("testUserLogin").password("testUserPassword"))
                .andExpect(status().isFound())
                .andExpect(authenticated().withUsername("testUserLogin"));
    }
}
