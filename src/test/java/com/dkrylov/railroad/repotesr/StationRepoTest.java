package com.dkrylov.railroad.repotesr;

import com.dkrylov.railroad.configuration.HibernateConfig;
import com.dkrylov.railroad.configuration.MyBeanConfig;
import com.dkrylov.railroad.domain.Station;
import com.dkrylov.railroad.repositories.StationRepo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, MyBeanConfig.class})
public class StationRepoTest {

    @Autowired
    private StationRepo stationRepo;

    @Test
    @Transactional
    @Rollback
    public void findStationBeNameTest() {
        Station station = new Station("testStationName");
        stationRepo.addStation(station);
        Assert.assertEquals(station, stationRepo.findStationByName(station.getName()));
    }
}
