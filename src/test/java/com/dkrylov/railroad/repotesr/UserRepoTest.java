package com.dkrylov.railroad.repotesr;

import com.dkrylov.railroad.configuration.HibernateConfig;
import com.dkrylov.railroad.configuration.MyBeanConfig;
import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.domain.UserRole;
import com.dkrylov.railroad.repositories.UserRepo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, MyBeanConfig.class})
public class UserRepoTest {

    @Autowired
    private UserRepo userRepo;

    @Test
    @Transactional
    @Rollback
    public void testAddUser() {
        User user = new User("testUserFirstName", "testUserLastName",
                LocalDate.of(2000, 01, 01), "testUserLogin",
                "testUserPassword", UserRole.ROLE_ADMIN);
        long id = userRepo.addUser(user);
        Assert.assertEquals(user, userRepo.findUserById(id));
    }

    @Test
    @Transactional
    @Rollback
    public void findUserByNameTest() {
        User user = new User("testUserFirstName", "testUserLastName",
                LocalDate.of(2000, 01, 01), "testUserLogin",
                "testUserPassword", UserRole.ROLE_ADMIN);
        userRepo.addUser(user);
        Assert.assertEquals(user, userRepo.findUserByLogin(user.getLogin()));

    }
}
