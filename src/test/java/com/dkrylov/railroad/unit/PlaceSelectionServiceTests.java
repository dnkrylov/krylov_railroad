package com.dkrylov.railroad.unit;

import com.dkrylov.railroad.domain.Carriage;
import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.dto.userdtos.CarriageDto;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.repositories.TrainRepo;
import com.dkrylov.railroad.services.PlaceSelectionServiceImpl;
import com.dkrylov.railroad.services.PriceCalculationService;
import com.dkrylov.railroad.services.TicketService;
import com.dkrylov.railroad.services.Validator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

public class PlaceSelectionServiceTests {
    @Mock
    private Validator validator;

    @Mock
    private TrainRepo trainRepo;

    @Mock
    private TicketService ticketService;

    @Mock
    private PriceCalculationService priceCalculationService;

    @InjectMocks
    private PlaceSelectionServiceImpl placeSelectionService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getTicketPriceTest() {
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setTrainId(1L);
        ticketInfo.setCarriageType("BUSINESS");
        ticketInfo.setStartStationId(1L);
        ticketInfo.setEndStationId(2L);
        when(priceCalculationService.getPrice(1L, CarriageType.BUSINESS, 1L, 2L))
                .thenReturn(33.33);
        Assert.assertEquals(33.33, placeSelectionService.getTicketPrice(ticketInfo), 0.01);
    }

    @Test
    public void createTemporaryTicketTest() {
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setTrainId(1L);
        when(ticketService.createTemporaryTicket(ticketInfo)).thenReturn(33L);
        Assert.assertEquals(33L, placeSelectionService.createTemporaryTicket(ticketInfo));
    }

    @Test
    public void getCarriagesTest(){
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setTrainId(1L);
        ticketInfo.setUserId(1L);
        ticketInfo.setStartStationId(1L);
        ticketInfo.setEndStationId(2L);
        Carriage carriage1 = new Carriage();
        carriage1.setId(1L);
        carriage1.setNumber("1");
        carriage1.setCarriageType(CarriageType.BUSINESS);
        carriage1.setCapacity(10);
        Carriage carriage2 = new Carriage();
        carriage2.setId(2L);
        carriage2.setNumber("2");
        carriage2.setCarriageType(CarriageType.COMFORT);
        carriage2.setCapacity(20);
        Carriage carriage3 = new Carriage();
        carriage3.setId(3L);
        carriage3.setNumber("3");
        carriage3.setCarriageType(CarriageType.ECONOMY);
        carriage3.setCapacity(30);
        when(trainRepo.findCarriagesByTrainId(1L)).thenReturn(Arrays.asList(carriage1,carriage2,carriage3));

        when(validator.isPlaceFree(1L, 1, 1L, 2L)).thenReturn(true);
        when(validator.isPlaceFree(1L, 2, 1L, 2L)).thenReturn(true);

        when(validator.isPlaceFree(2L, 1, 1L, 2L)).thenReturn(true);
        when(validator.isPlaceFree(2L, 2, 1L, 2L)).thenReturn(true);
        when(validator.isPlaceFree(2L, 3, 1L, 2L)).thenReturn(true);

        when(validator.isPlaceFree(3L, 1, 1L, 2L)).thenReturn(true);
        when(validator.isPlaceFree(3L, 2, 1L, 2L)).thenReturn(true);
        when(validator.isPlaceFree(3L, 3, 1L, 2L)).thenReturn(true);
        when(validator.isPlaceFree(3L, 4, 1L, 2L)).thenReturn(true);

        List<CarriageDto> carriageDtos = placeSelectionService.getCarriages(ticketInfo);
        Assert.assertEquals(3, carriageDtos.size());
        Assert.assertEquals(2,carriageDtos.get(0).getAvailablePlaces());
        Assert.assertEquals(3,carriageDtos.get(1).getAvailablePlaces());
        Assert.assertEquals(4, carriageDtos.get(2).getAvailablePlaces());

        Assert.assertTrue(carriageDtos.get(0).getFreePlaceNumbers().contains(2));
        Assert.assertFalse(carriageDtos.get(0).getFreePlaceNumbers().contains(3));

        Assert.assertTrue(carriageDtos.get(1).getFreePlaceNumbers().contains(3));
        Assert.assertFalse(carriageDtos.get(1).getFreePlaceNumbers().contains(4));

        Assert.assertTrue(carriageDtos.get(2).getFreePlaceNumbers().contains(4));
        Assert.assertFalse(carriageDtos.get(2).getFreePlaceNumbers().contains(5));
    }
}
