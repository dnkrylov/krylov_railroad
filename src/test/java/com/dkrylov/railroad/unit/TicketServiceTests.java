package com.dkrylov.railroad.unit;

import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.repositories.StationRepo;
import com.dkrylov.railroad.repositories.TicketRepo;
import com.dkrylov.railroad.repositories.TrainRepo;
import com.dkrylov.railroad.repositories.UserRepo;
import com.dkrylov.railroad.services.TicketServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

public class TicketServiceTests {
    @Mock
    private TicketRepo ticketRepo;

    @Mock
    private UserRepo userRepo;

    @Mock
    private TrainRepo trainRepo;

    @Mock
    private StationRepo stationRepo;

    @InjectMocks
    private TicketServiceImpl ticketService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createTemporaryTicketTest() {
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setCarriageId(1L);
        ticketInfo.setUserId(1L);
        Assert.assertEquals(0L, ticketService.createTemporaryTicket(ticketInfo));
    }

    @Test
    public void upgradeTemporaryTicketTest() {
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setTicketId(1L);
        when(ticketRepo.findTicketByIdForUpgrade(1L)).thenReturn(true);
        when(ticketRepo.findTicketByIdForUpgrade(2L)).thenReturn(false);
        Assert.assertTrue(ticketService.upgradeTemporaryTicket(ticketInfo));
        ticketInfo.setTicketId(2L);
        Assert.assertFalse(ticketService.upgradeTemporaryTicket(ticketInfo));
    }

}
