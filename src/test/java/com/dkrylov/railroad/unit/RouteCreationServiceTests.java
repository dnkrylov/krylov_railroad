package com.dkrylov.railroad.unit;

import com.dkrylov.railroad.domain.RouteNode;
import com.dkrylov.railroad.domain.Station;
import com.dkrylov.railroad.domain.Train;
import com.dkrylov.railroad.dto.userdtos.AvailableRouteDto;
import com.dkrylov.railroad.repositories.RouteNodeRepo;
import com.dkrylov.railroad.repositories.StationRepo;
import com.dkrylov.railroad.services.RouteCreationServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

public class RouteCreationServiceTests {
    @Mock
    private RouteNodeRepo routeNodeRepo;
    @Mock
    private StationRepo stationRepo;
    @InjectMocks
    private RouteCreationServiceImpl routeCreationService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAvailableRoutesTest(){
        Station station1 = new Station(1L, "1");
        Station station2 = new Station(2L, "2");
        when(stationRepo.findStationByName("1")).thenReturn(station1);
        when(stationRepo.findStationByName("2")).thenReturn(station2);
        Train train = new Train();
        train.setName("train");
        train.setId(1L);
        LocalDateTime startPeriod = LocalDateTime.of(1111,11,11,11,11);
        LocalDateTime endPeriod = LocalDateTime.of(2111,11,11,11,11);
        when(routeNodeRepo.findTrainsByStartStationEndStationAndStartPeriod(station1,station2, startPeriod,endPeriod))
                .thenReturn(Collections.singletonList(train));

        RouteNode startNode = new RouteNode();
        startNode.setDepartureTime(LocalDateTime.of(2001,11,11,11,11));
        startNode.setTrain(train);
        RouteNode endNode = new RouteNode();
        endNode.setArrivalTime(LocalDateTime.of(2002,11,11,11,11));

        when(routeNodeRepo.findRouteNodeByTrainAndStation(train, station1)).thenReturn(startNode);
        when(routeNodeRepo.findRouteNodeByTrainAndStation(train, station2)).thenReturn(endNode);

        List<AvailableRouteDto> routes = routeCreationService.findAvailableRoutes("1","2",
                "11.11.1111:11.11", "11.11.2111:11.11");
        long trainId = routes.get(0).getTrainId();
        Assert.assertEquals(1L, trainId);

        //reverse route (no trains should be found)
        List<AvailableRouteDto> emptyRoutes = routeCreationService.findAvailableRoutes("2","1",
                "11.11.1111:11.11", "11.11.2111:11.11");
        Assert.assertEquals(0, emptyRoutes.size());

        //arriving from startStation after Departing to end. Train follows reverse way. No trains should be found
        startNode.setDepartureTime(LocalDateTime.of(2003,11,11,11,11));
        List<AvailableRouteDto> emptyRoutes2 = routeCreationService.findAvailableRoutes("1","2",
                "11.11.1111:11.11", "11.11.2111:11.11");
        Assert.assertEquals(0, emptyRoutes2.size());
    }
}
