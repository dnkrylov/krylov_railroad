package com.dkrylov.railroad.unit;

import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.repositories.UserRepo;
import com.dkrylov.railroad.services.RegistrationServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

public class RegistrationServiceTests {
    @Mock
    private UserRepo userRepo;

    @InjectMocks
    private RegistrationServiceImpl registrationService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isUserNameValidTest(){
        User user = new User();
        when(userRepo.findUserByLogin("exists")).thenReturn(user);
        when(userRepo.findUserByLogin("notExists")).thenReturn(null);
        Assert.assertFalse(registrationService.isUserLoginFree("exists"));
        Assert.assertTrue(registrationService.isUserLoginFree("notExists"));
    }
}
