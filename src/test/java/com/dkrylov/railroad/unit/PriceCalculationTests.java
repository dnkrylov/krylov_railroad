package com.dkrylov.railroad.unit;

import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.repositories.PriceRepo;
import com.dkrylov.railroad.repositories.RouteNodeRepo;
import com.dkrylov.railroad.services.PriceCalculationServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

public class PriceCalculationTests {

    @Mock
    private PriceRepo priceRepo;
    @Mock
    private RouteNodeRepo routeNodeRepo;
    @InjectMocks
    private PriceCalculationServiceImpl priceCalculationService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getPriceTest(){
        List<Long> sortedIds = Arrays.asList(1L, 2L, 3L,4L);

        when(routeNodeRepo.findSortedStationIdsByTrainId(1L)).thenReturn(sortedIds);

        when(priceRepo.findPriceByTrainIdCarriageTypeStartStationIdEndStationId(1L,
                CarriageType.BUSINESS,1L, 2L)).thenReturn(11.11);
        when(priceRepo.findPriceByTrainIdCarriageTypeStartStationIdEndStationId(1L,
                CarriageType.BUSINESS,2L, 3L)).thenReturn(11.11);
        when(priceRepo.findPriceByTrainIdCarriageTypeStartStationIdEndStationId(1L,
                CarriageType.BUSINESS,3L, 4L)).thenReturn(11.11);
        Assert.assertEquals(11.11, priceCalculationService.getPrice(1L, CarriageType.BUSINESS,
                1L,2L), 0.01);
        Assert.assertEquals(22.22, priceCalculationService.getPrice(1L, CarriageType.BUSINESS,
                1L,3L), 0.01);
        Assert.assertEquals(33.33, priceCalculationService.getPrice(1L, CarriageType.BUSINESS,
                1L,4L), 0.01);
        Assert.assertEquals(22.22, priceCalculationService.getPrice(1L, CarriageType.BUSINESS,
                2L,4L), 0.01);
    }
}
