package com.dkrylov.railroad.unit;

import com.dkrylov.railroad.domain.*;
import com.dkrylov.railroad.dto.admin.CarriageForTrainCreationDto;
import com.dkrylov.railroad.dto.admin.RouteCreationDto;
import com.dkrylov.railroad.dto.admin.TrainCreationDto;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.exceptions.InvalidRequestParamException;
import com.dkrylov.railroad.repositories.RouteNodeRepo;
import com.dkrylov.railroad.repositories.StationRepo;
import com.dkrylov.railroad.repositories.TicketRepo;
import com.dkrylov.railroad.repositories.TrainRepo;
import com.dkrylov.railroad.services.ValidatorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

public class ValidatorTests {
    private static final Station STATION1 = new Station(1L,"1");
    private static final Station STATION2 = new Station(2L,"2");
    private static final Carriage CARRIAGE = new Carriage();
    private static final User USER1 = new User();
    private static final TrainCreationDto TRAIN_FOR_VALIDATION = new TrainCreationDto();

    @Mock
    private TicketRepo ticketRepo;
    @Mock
    private RouteNodeRepo routeNodeRepo;
    @Mock
    private TrainRepo trainRepo;
    @Mock
    private StationRepo stationRepo;

    @InjectMocks
    private ValidatorImpl validator;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
        TRAIN_FOR_VALIDATION.setNumber("testTrain");
        TRAIN_FOR_VALIDATION.setStationList(new ArrayList<>());
        TRAIN_FOR_VALIDATION.setCarriages(new ArrayList<>());
        TRAIN_FOR_VALIDATION.getCarriages().add(0,
                new CarriageForTrainCreationDto("1", "BUSINESS", "33"));
        TRAIN_FOR_VALIDATION.getCarriages().add(1,
                new CarriageForTrainCreationDto("2", "COMFORT", "33"));
        TRAIN_FOR_VALIDATION.getStationList().add(0, new RouteCreationDto("station1",
                "11.11.1111:11.11","11.11.1111:11.12", "1.03",
                "1.04", "1.05"));
        TRAIN_FOR_VALIDATION.getStationList().add(1, new RouteCreationDto("station2",
                "11.11.1111:22.11","11.11.1111:22.12", "1.03",
                "1.04", "1.05"));
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void placeValidationTickets(){
        when(ticketRepo.findTicketsByCarriageIdAndPlaceNumber(1,1))
                .thenReturn(new ArrayList<>());
        Assert.assertTrue(validator.isPlaceFree(1L,1, 1L,2L));

        CARRIAGE.setId(1L);
        Ticket t1 = new Ticket(1L,2,USER1,CARRIAGE,STATION1,STATION2,100.00, LocalDateTime.now());
        List<Ticket> tickets = Collections.singletonList(t1);
        when(ticketRepo.findTicketsByCarriageIdAndPlaceNumber(1L,2)).thenReturn(tickets);
        when(routeNodeRepo.findSortedStationIdsByTrainId(1L)).thenReturn(Arrays.asList(1L, 2L,3L));
        when(trainRepo.findTrainIdByCarriageId(1L)).thenReturn(1L);
        Assert.assertFalse(validator.isPlaceFree(1L,2, 1L,2L));
        Assert.assertTrue(validator.isPlaceFree(1L,2, 2L,3L));
        Assert.assertFalse(validator.isPlaceFree(1L,2, 1L,3L));

        Ticket t2 = new Ticket(1L,3,USER1,CARRIAGE,STATION1,STATION2,-100.00, LocalDateTime.now());
        List<Ticket> tickets2 = Collections.singletonList(t2);
        when(ticketRepo.findTicketsByCarriageIdAndPlaceNumber(1L,3)).thenReturn(tickets2);
        Assert.assertFalse(validator.isPlaceFree(1L,3, 2L,3L));
        Assert.assertFalse(validator.isPlaceFree(1L,3, 1L,2L));
    }

    @Test
    public void validateTimeTest() {
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setStartStationId(1L);
        ticketInfo.setTrainId(1L);
        RouteNode routeNode = new RouteNode();
        routeNode.setDepartureTime(LocalDateTime.now().plusMinutes(5));
        when(routeNodeRepo.findDepartureTimeByStationIdAndTrainId(1L, 1L)).thenReturn(routeNode);
        Assert.assertFalse(validator.validateTime(ticketInfo));

        ticketInfo.setStartStationId(2L);
        routeNode.setDepartureTime(LocalDateTime.now().plusMinutes(11));
        when(routeNodeRepo.findDepartureTimeByStationIdAndTrainId(2L, 1L)).thenReturn(routeNode);
        Assert.assertTrue(validator.validateTime(ticketInfo));
    }

    @Test
    public void validateTicketTests(){
        CARRIAGE.setId(1L);
        when(trainRepo.findCarriagesByTrainId(1L)).thenReturn(Collections.singletonList(CARRIAGE));
        when(ticketRepo.findTicketsByCarriageIdAndUserId(1L, 1L)).thenReturn(new ArrayList<>());
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setTrainId(1L);
        ticketInfo.setUserId(1L);
        Assert.assertTrue(validator.validateTicket(ticketInfo));

        CARRIAGE.setId(2L);
        when(trainRepo.findCarriagesByTrainId(2L)).thenReturn(Collections.singletonList(CARRIAGE));
        when(ticketRepo.findTicketsByCarriageIdAndUserId(2L, 1L))
                .thenReturn(Arrays.asList(new Ticket(), new Ticket()));
        ticketInfo.setTrainId(2L);
        Assert.assertFalse(validator.validateTicket(ticketInfo));

        CARRIAGE.setId(3L);
        when(trainRepo.findCarriagesByTrainId(3L)).thenReturn(Collections.singletonList(CARRIAGE));
        Ticket falseTicket = new Ticket();
        falseTicket.setPrice(100.77);
        when(ticketRepo.findTicketsByCarriageIdAndUserId(3L, 1L))
                .thenReturn(Collections.singletonList(falseTicket));
        ticketInfo.setTrainId(3L);
        Assert.assertFalse(validator.validateTicket(ticketInfo));

        CARRIAGE.setId(4L);
        when(trainRepo.findCarriagesByTrainId(4L)).thenReturn(Collections.singletonList(CARRIAGE));
        Ticket trueTicket = new Ticket();
        trueTicket.setPrice(-200.00);
        when(ticketRepo.findTicketsByCarriageIdAndUserId(4L, 1L))
                .thenReturn(Collections.singletonList(trueTicket));
        ticketInfo.setTrainId(4L);
        Assert.assertTrue(validator.validateTicket(ticketInfo));
    }

    @Test
    public void isStationWithSuchNameExistsTest() {
        when(stationRepo.findStationByName("1")).thenReturn(STATION1);
        Assert.assertTrue(validator.isStationWithSuchNameExists("1"));
        Assert.assertFalse(validator.isStationWithSuchNameExists("NotExistingStation"));
    }

    @Test
    public void validateReceivedValuesTest() {
        TrainCreationDto trainDto = new TrainCreationDto();
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("train number should not be empty");
        Assert.assertTrue(validator.validateReceivedTrainValues(trainDto));
    }

    @Test
    public void validateReceivedValuesTest2() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("");
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("train number should not be empty");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest3() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("train should have at least 1 carriage");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest31() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto(
                "carName", "BUSINESS", "11")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("train should have at least 2 stations");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest4() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto("111",
                "11.11.1111:11.11","11.11.1111:11.11",
                "1111", "23", "22.22")));
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto(null,
                "BUSINESS", "11")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("carriage number should not be empty");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest5() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto("111",
                "11.11.1111:11.11","11.11.1111:11.11",
                "1111", "23", "22.22")));
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto("22",
                "wrongCarType", "11")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("invalid type value");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest6() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto("111",
                "11.11.1111:11.11","11.11.1111:11.11",
                "1111", "23", "22.22")));
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto("22",
                "BUSINESS", "00")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("Should be from 1 to 999");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest7() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto(
                "1", "BUSINESS", "33")));
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto(null,
                "11.11.1111:11.11","11.11.1111:11.11", "24", "23",
                "22.22")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("station name should not be empty");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest8() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto(
                "1", "BUSINESS", "33")));
        String stationName = "testStation";
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto(stationName,
                "-11.11.1111:11.11","11.11.1111:11.11",
                "24", "23", "22.22")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("invalid arrival/departure time format");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest9() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto(
                "1", "BUSINESS", "33")));
        String stationName = "testStation";
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto(stationName, null,
                "11.11.1111:11.11", "24", "23", "22.22")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("arrival/departure should not be empty");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest10() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto(
                "1", "BUSINESS", "33")));
        String stationName = "testStation";
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto(stationName,
                "11.11.1111:11.11","11.11.1111:11.11", null,
                "23", "22.22")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("price should not be empty");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest11() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto(
                "1", "BUSINESS", "33")));
        String stationName = "testStation";
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto(stationName,
                "11.11.1111:11.11","11.11.1111:11.11", "999999999.99",
                "23", "22.22")));
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("invalid price format");
        validator.validateReceivedTrainValues(trainDto);
    }

    @Test
    public void validateReceivedValuesTest12() {
        TrainCreationDto trainDto = new TrainCreationDto();
        trainDto.setNumber("1");
        trainDto.setCarriages(Collections.singletonList(new CarriageForTrainCreationDto(
                "1", "BUSINESS", "33")));
        String stationName = "testStation";
        trainDto.setStationList(Collections.singletonList(new RouteCreationDto(stationName,
                "11.11.1111:11.11","11.11.1111:11.11", "1",
                "23", "22.22")));
        Assert.assertTrue(validator.validateReceivedTrainValues(trainDto));
    }

    @Test
    public void validateTrainParametersTest() {
        when(stationRepo.findStationByName("station1")).thenReturn(new Station("existingStation"));
        when(stationRepo.findStationByName("station2")).thenReturn(new Station("existingStation"));
        TRAIN_FOR_VALIDATION.getCarriages().get(0).setCarName("2");
        Assert.assertEquals("duplicate carriage numbers: "
                        + TRAIN_FOR_VALIDATION.getCarriages().get(0).getCarName(),
                validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.getCarriages().get(0).setCarName("1");

        List<CarriageForTrainCreationDto> temp = TRAIN_FOR_VALIDATION.getCarriages();
        TRAIN_FOR_VALIDATION.setCarriages(null);
        Assert.assertEquals("train should have at least one carriage",
                validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.setCarriages(new ArrayList<>());
        Assert.assertEquals("train should have at least one carriage",
                validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.setCarriages(temp);

        List<RouteCreationDto> temp2 = TRAIN_FOR_VALIDATION.getStationList();
        TRAIN_FOR_VALIDATION.setStationList(null);
        Assert.assertEquals("train should have at least two stations",
                validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.setStationList(new ArrayList<>());
        Assert.assertEquals("train should have at least two stations",
                validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.setStationList(temp2);

        TRAIN_FOR_VALIDATION.getStationList().get(0).setStationName("station2");
        Assert.assertEquals("duplicate stations: " + TRAIN_FOR_VALIDATION.getStationList().get(0).getStationName(),
                validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.getStationList().get(0).setStationName("station1");

        TRAIN_FOR_VALIDATION.getStationList().get(0).setStationName("notExistingStation");
        Assert.assertEquals("notExistingStation: station does not exist",
                validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.getStationList().get(0).setStationName("station1");

        TRAIN_FOR_VALIDATION.getStationList().get(0).setDepartureTime("11.11.1111:22.12");
        Assert.assertEquals("duplicate departure time: " + TRAIN_FOR_VALIDATION.getStationList().get(0)
                        .getDepartureTime(), validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.getStationList().get(0).setDepartureTime("11.11.1111:11.12");

        TRAIN_FOR_VALIDATION.getStationList().get(1).setArrivingTime("11.11.1111:11.11");
        Assert.assertEquals("duplicate arriving time: " + TRAIN_FOR_VALIDATION.getStationList().get(1)
                .getArrivingTime(), validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.getStationList().get(1).setArrivingTime("11.11.1111:22.11");

        TRAIN_FOR_VALIDATION.getStationList().get(0).setArrivingTime("11.11.1111:11.13");
        Assert.assertEquals(TRAIN_FOR_VALIDATION.getStationList().get(0)
                .getStationName() + ": departing before arriving", validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
        TRAIN_FOR_VALIDATION.getStationList().get(0).setArrivingTime("11.11.1111:11.11");

        Assert.assertEquals("CORRECT", validator.validateTrainParameters(TRAIN_FOR_VALIDATION));
    }

    @Test
    public void validateCardParametersTests() {
        Assert.assertEquals("Invalid credit card number",
                validator.validateCardParameters("134", "11/11", "333"));
        Assert.assertEquals("Invalid credit card number",
                validator.validateCardParameters("111122223333....", "11/11", "333"));
        Assert.assertEquals("Invalid credit card expiration",
                validator.validateCardParameters("1234567812345678", "13/11", "333"));
        Assert.assertEquals("Invalid credit card expiration",
                validator.validateCardParameters("1234567812345678", "afs11", "333"));
        Assert.assertEquals("Invalid credit card cvv",
                validator.validateCardParameters("1234567812345678", "11/11", "3353"));
        Assert.assertEquals("Invalid credit card cvv",
                validator.validateCardParameters("1234567812345678", "11/11", "3x3"));
        Assert.assertEquals("CORRECT",
                validator.validateCardParameters("1234567812345678", "11/11", "333"));
    }
}
