package com.dkrylov.railroad.unit;

import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.dto.userdtos.UserDetailsDto;
import com.dkrylov.railroad.repositories.UserRepo;
import com.dkrylov.railroad.services.UserDetailsServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.mockito.Mockito.when;

public class UserDetailsServiceTests {

    @Mock
    private UserRepo userRepo;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void loadUserByUsernameTest1() {
        User user = new User();
        when(userRepo.findUserByLogin("exists")).thenReturn(user);
        UserDetailsDto userDetailsDto = new UserDetailsDto(user);
        Assert.assertEquals(userDetailsService.loadUserByUsername("exists"), userDetailsDto);
    }

    @Test
    public void loadUserByUsernameTest2() {
        String name = "notExists";
        when(userRepo.findUserByLogin(name)).thenReturn(null);
        expectedException.expect(UsernameNotFoundException.class);
        expectedException.expectMessage("User: " + name + " not found");
        userDetailsService.loadUserByUsername(name);
    }
}
