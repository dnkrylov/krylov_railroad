package com.dkrylov.railroad;

import com.dkrylov.railroad.configuration.HibernateConfig;
import com.dkrylov.railroad.configuration.MyBeanConfig;
import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.domain.RouteNode;
import com.dkrylov.railroad.domain.Ticket;
import com.dkrylov.railroad.domain.Train;
import com.dkrylov.railroad.repositories.*;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, MyBeanConfig.class})
public class Test1 {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private RouteNodeRepo routeNodeRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TicketRepo ticketRepo;
    @Autowired
    private TrainRepo trainRepo;

    @Test
@Transactional
    public void test() {
        List<Long> list = new ArrayList<>();
        list.add(2L);
        list.add(3L);
        System.out.println(ticketRepo.findTicketsByCarriageIds(list));
    }
}
