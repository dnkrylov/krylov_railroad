DROP TABLE IF EXISTS js_price;
DROP TABLE IF EXISTS js_route;
DROP TABLE IF EXISTS js_ticket;
DROP TABLE IF EXISTS js_carriage;
DROP TABLE IF EXISTS js_train;
DROP TABLE IF EXISTS js_station;
DROP TABLE IF EXISTS js_user;

CREATE TABLE js_user
(
    user_id bigserial not null,
    user_first_name varchar(100) not null,
    user_last_name varchar(100) not null,
    user_date_of_birth timestamp not null,
    user_login varchar(100) unique not null,
    user_password varchar(100) not null,
    user_role varchar(100) not null,
    PRIMARY KEY (user_id)
);

CREATE TABLE js_train
(
    train_id bigserial not null,
    train_name varchar(100) not null,
    PRIMARY KEY (train_id)
);

CREATE TABLE js_carriage
(
    carriage_id bigserial not null,
    train_id bigint not null,
    carriage_number varchar(100) not null,
    carriage_type varchar(100) not null,
    carriage_capacity integer not null,
    PRIMARY KEY (carriage_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE js_station
(
    station_id bigserial not null,
    station_name varchar(300) unique not null,
    PRIMARY KEY (station_id)
);


CREATE TABLE js_ticket
(
    ticket_id bigserial not null,
    carriage_id bigint not null,
    place_number integer not null,
    user_id bigint not null,
    start_station_id bigint,
    finish_station_id bigint,
    ticket_price decimal,
    creation_time timestamp not null,
    PRIMARY KEY (ticket_id),
    FOREIGN KEY (carriage_id) REFERENCES js_carriage(carriage_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (start_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (finish_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES js_user(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
CREATE UNIQUE INDEX ticket_constraint ON js_ticket (carriage_id, place_number) WHERE (ticket_price < 0);

CREATE TABLE js_price
(
    price_part_id bigserial not null,
    carriage_type varchar(100) not null,
    train_id bigint not null,
    start_station_id bigint not null,
    finish_station_id bigint not null,
    price decimal,
    PRIMARY KEY (price_part_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (start_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (finish_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    UNIQUE (carriage_type, train_id, start_station_id, finish_station_id)
);


CREATE TABLE js_route
(
    route_node_id bigserial not null,
    train_id bigserial not null,
    station_id bigint not null,
    arrival_time timestamp not null,
    departure_time timestamp not null,
    PRIMARY KEY (route_node_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (station_id) REFERENCES js_station(station_id) ON DELETE CASCADE ON UPDATE CASCADE,
    UNIQUE (train_id, station_id)
);