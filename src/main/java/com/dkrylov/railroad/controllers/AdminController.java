package com.dkrylov.railroad.controllers;

import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.dto.admin.*;
import com.dkrylov.railroad.exceptions.InvalidRequestParamException;
import com.dkrylov.railroad.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@Controller
public class AdminController {
    private static final String DATE_FORMAT_PATTERN =
            "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$";

    @Autowired
    private AdminService adminService;

    @GetMapping(value = "/admin/showAdminPage")
    public String showAdminPage() {
        return "admin/adminPage";
    }

    @GetMapping(value = "/admin/getStations")
    @ResponseBody
    public List<String> getStations() {
        return adminService.getStationNamesList();
    }

    @GetMapping(value = "/admin/getCarriageTypes")
    @ResponseBody
    public List<CarriageType> getCarriageTypes() {
        return Arrays.asList(CarriageType.class.getEnumConstants());
    }

    @PostMapping(value = "/admin/addTrain", produces = "application/json")
    @ResponseBody
    public BusinessResponse saveTrain(@RequestBody TrainCreationDto trainCreationDto) {
        adminService.validateReceivedValues(trainCreationDto);
        BusinessResponse response = new BusinessResponse();
        String result = adminService.addTrain(trainCreationDto);
        response.setMessage(result);
        return response;
    }

    @PostMapping(value = "/admin/addStation", produces = "application/json")
    @ResponseBody
    public BusinessResponse saveStation(@RequestBody StationDto stationDto) {
        if (stationDto.getName() == null || stationDto.getName().equals("")) {
            throw new InvalidRequestParamException("station number should not be empty");
        }
        BusinessResponse response = new BusinessResponse();
        if (!adminService.addStation(stationDto)) {
            response.setMessage("Station not created. Station with same name already exist!");
            return response;
        } else {
            response.setMessage("Station '" + stationDto.getName() + "' successfully created!");
            return response;
        }
    }

    @GetMapping(value = "/admin/getTrains", produces = "application/json")
    @ResponseBody
    public List<AdminTimeTableItemDto> getTrains(@RequestParam(name = "date") String date,
                                            @RequestParam(name = "station") String stationName) {
        if (!date.matches(DATE_FORMAT_PATTERN)) {
            return null;
        }
        List<AdminTimeTableItemDto> timeTable = adminService.getTimeTable(stationName, date);
        return timeTable;
    }

    @GetMapping(value = "/admin/getPassengers", produces = "application/json")
    @ResponseBody
    public List<UserForAdminRequestDto> getPassengers(@RequestParam(name = "trainId") String trainId) {
        if (!trainId.matches("^[0-9]+$")) {
            return null;
        }
        List<UserForAdminRequestDto> list = adminService.getPassengers(Long.parseLong(trainId));
        System.out.println(list);
        return adminService.getPassengers(Long.parseLong(trainId));
    }
}
