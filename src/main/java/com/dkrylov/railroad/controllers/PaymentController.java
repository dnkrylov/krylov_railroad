package com.dkrylov.railroad.controllers;

import com.dkrylov.railroad.dto.admin.BusinessResponse;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.dto.userdtos.RouteInfo;
import com.dkrylov.railroad.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;

@Controller
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @GetMapping(value = "/registered/showPaymentPage/")
    public String showPaymentPage(Model model, HttpSession session) {
        RouteInfo routeInfo = paymentService.fillRouteInfo((TicketInfo) session.getAttribute("ticketInfo"));
        model.addAttribute("routeInfo", routeInfo);
        return "registered/paymentPage";
    }

    @PostMapping(value = "/registered/tryToBuyTicket/")
    @ResponseBody
    public BusinessResponse tryToBuyTicket(HttpSession session, @RequestParam(name = "cardNumber") String cardNumber,
                                           @RequestParam(name = "cardExp") String cardExp,
                                           @RequestParam(name = "cardCVV") String cardCVV) {
        TicketInfo ticketInfo = (TicketInfo) session.getAttribute("ticketInfo");
        BusinessResponse response = new BusinessResponse();
        String cardParametersValidationResult = paymentService.validateCardParameters(cardNumber, cardExp, cardCVV);
        if(!cardParametersValidationResult.equals("CORRECT")){
            response.setMessage(cardParametersValidationResult);
            return response;
        }
        String ticketUpgradingResult = paymentService.upgradeTemporaryTicket(ticketInfo);
        switch (ticketUpgradingResult) {
            case "DEPARTED":
                response.setMessage("TICKET NOT BOUGHT! \n THIS TRAIN IS ALREADY DEPARTED/DEPARTING IN 10 MINUTES");
                break;
            case "DUPLICATE":
                response.setMessage("TICKET NOT BOUGHT! YOU ALREADY HAVE TICKET TO THIS TRAIN");
                break;
            case "SUCCESS":
                response.setMessage("SUCCESS! YOU HAVE BOUGHT A TICKET ");
                break;
            case "LATE":
                response.setMessage("SORRY, YOU ARE LATE, THIS TICKET WAS ALREADY BOUGHT");
                break;
        }
        return response;
    }
}
