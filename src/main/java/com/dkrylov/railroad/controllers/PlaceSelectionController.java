package com.dkrylov.railroad.controllers;

import com.dkrylov.railroad.dto.userdtos.CarriageDto;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.services.PlaceSelectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class PlaceSelectionController {

    @Autowired
    private PlaceSelectionService placeSelectionService;

    @ModelAttribute(name = "carriages")
    public List<CarriageDto> carriageDtos(@SessionAttribute(name = "ticketInfo") TicketInfo ticketInfo){
        return placeSelectionService.getCarriages(ticketInfo);
    }

    @GetMapping(value = "/registered/showCarriages")
    public String showUserForm() {
        return "registered/placeSelectionPage";
    }

    @PostMapping(value = "/registered/goToPaymentPage")
    public String preparePaymentPage(HttpSession session,
                                     @RequestParam(name = "carriageId") long carriageId,
                                     @RequestParam(name = "placeNumber") int placeNumber,
                                     @RequestParam(name = "carriageType") String carriageType){
        TicketInfo ticketInfo = (TicketInfo) session.getAttribute("ticketInfo");
        ticketInfo.setCarriageId(carriageId);
        ticketInfo.setCarriageType(carriageType);
        ticketInfo.setPlaceNumber(placeNumber);
        ticketInfo.setTicketPrice(placeSelectionService.getTicketPrice(ticketInfo));
        try{
            ticketInfo.setTicketId(placeSelectionService.createTemporaryTicket(ticketInfo));
        } catch (DataIntegrityViolationException e){
            return "registered/placeSelectionPage";
        }

        return "redirect:/registered/showPaymentPage/";
    }
}
