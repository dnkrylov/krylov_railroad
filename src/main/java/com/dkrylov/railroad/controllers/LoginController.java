package com.dkrylov.railroad.controllers;

import com.dkrylov.railroad.dto.userdtos.UserDetailsDto;
import com.dkrylov.railroad.dto.userdtos.UserDto;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
    @GetMapping(value = "/unregistered/showLoginForm")
    public String showLoginForm() {
        return "/unregistered/loginForm";
    }

    @GetMapping(value = "/unregistered/loginFailed")
    public String loginError(Model model) {
        model.addAttribute("error", "Incorrect login or password");
        return "/unregistered/loginForm";
    }

    @GetMapping(value = "/unregistered/logout")
    public String logout(SessionStatus sessionStatus) {
        SecurityContextHolder.getContext().setAuthentication(null);
        sessionStatus.setComplete();
        return "redirect:/unregistered/showLoginForm";
    }

    @GetMapping(value = "/registered/afterLogin")
    public String setSessionConfig(HttpSession session) {
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken)SecurityContextHolder
                .getContext().getAuthentication();
        UserDetailsDto loggedInUser = (UserDetailsDto)authentication.getPrincipal();
        UserDto userDto = new UserDto(loggedInUser.getUser());
        session.setAttribute("userId", userDto.getUserId());
        return "redirect:/registered/showHomePageForm";
    }
}
