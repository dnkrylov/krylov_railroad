package com.dkrylov.railroad.controllers;

import com.dkrylov.railroad.dto.userdtos.AvailableRouteDto;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.dto.userdtos.TimeTableItemDto;
import com.dkrylov.railroad.exceptions.InvalidRequestParamException;
import com.dkrylov.railroad.services.OrdinaryUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class OrdinaryUserController {
    private static final String dateFormatPattern =
            "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$";

    @Autowired
    private OrdinaryUserService ordinaryUserService;

    @GetMapping(value = "/registered/getStations")
    @ResponseBody
    public List<String> getStations() {
        return ordinaryUserService.getStationNamesList();
    }

    @GetMapping(value = "/registered/showHomePageForm")
    public String showUserForm() {
        return "registered/ordinaryUserPage";
    }

    @GetMapping(value = "/registered/showTimeTable", produces = "application/json")
    @ResponseBody
    public List<TimeTableItemDto> showTimeTable(@RequestParam(name = "date") String date,
                                                @RequestParam(name = "station") String stationName) {
        if (!date.matches(dateFormatPattern)) {
            throw new InvalidRequestParamException("Invalid date format. Date should be dd.mm.yyyy formatted");
        }
        List<TimeTableItemDto> timeTable = ordinaryUserService.getTimeTableItems(stationName, date);
        return timeTable;
    }

    @GetMapping(value = "/registered/showRoutes", produces = "application/json")
    @ResponseBody
    public List<AvailableRouteDto> showAvailableRoutes(HttpSession session,
                                                       @RequestParam(name = "startStationName") String startStationName,
                                                       @RequestParam(name = "endStationName") String endStationName,
                                                       @RequestParam(name = "startPeriod") String startPeriod,
                                                       @RequestParam(name = "endPeriod") String endPeriod) {
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setUserId((long) session.getAttribute("userId"));
        ticketInfo.setStartStationId(ordinaryUserService.findStationIdByName(startStationName));
        ticketInfo.setEndStationId(ordinaryUserService.findStationIdByName(endStationName));
        session.setAttribute("ticketInfo", ticketInfo);
        return ordinaryUserService.findRoutes(startStationName, endStationName, startPeriod, endPeriod);
    }

    @PostMapping(value = "/registered/prepareCarriagesForm/")
    public String showUserForm(HttpSession session, @RequestParam(name = "selectedTrain") long trainId) {
        TicketInfo ticketInfo = (TicketInfo)session.getAttribute("ticketInfo");
        ticketInfo.setTrainId(trainId);
        return "redirect:/registered/showCarriages/";
    }
}
