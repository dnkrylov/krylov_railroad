package com.dkrylov.railroad.controllers;

import com.dkrylov.railroad.dto.userdtos.UserDto;
import com.dkrylov.railroad.services.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Controller
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @GetMapping(value = "/unregistered/showRegistrationForm")
    public String registration(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "unregistered/registerForm";
    }

    @PostMapping(value = "/unregistered/tryToRegister")
    public String tryToRegister(@Valid @ModelAttribute("userDto") UserDto userDto, BindingResult result) {
        if (!registrationService.isUserLoginFree(userDto.getLogin())) {
            result.rejectValue("login", "error.username", "user with same login already exists");
        }
        if (result.hasErrors()) {
            return "unregistered/registerForm";
        }
        registrationService.addUser(userDto);
        return "redirect:/unregistered/showLoginForm";
    }
}



