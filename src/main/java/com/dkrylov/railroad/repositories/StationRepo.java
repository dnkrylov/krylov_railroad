package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.Station;
import java.util.List;

public interface StationRepo {
    Station findStationById(long id);
    List<Station> findAllStations();
    Station findStationByName(String name);
    long addStation(Station station);
}
