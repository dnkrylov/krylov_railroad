package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.Carriage;
import com.dkrylov.railroad.domain.Train;
import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.exceptions.DatabaseConfigurationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TrainRepoImpl implements TrainRepo {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public Train findTrainById(long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Train.class, id);
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public List<Carriage> findCarriagesByTrainId(long trainId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Carriage> query = session.createQuery("FROM Carriage c WHERE c.train.id = :trainId");
        query.setParameter("trainId", trainId);
        return query.getResultList();
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public long findTrainIdByCarriageId(long carriageId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Long> query = session.createQuery("select train.id FROM Carriage c WHERE c.id = :carriageId");
        query.setParameter("carriageId", carriageId);
        List<Long> ids = query.list();
        switch (ids.size()) {
            case 0: throw new DatabaseConfigurationException("train with such carriage not found");
            case 1: return ids.get(0);
            default: throw new DatabaseConfigurationException("more then one train has one carriage");
        }
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public Carriage findCarriageById(long carriageId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Carriage.class, carriageId);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public long addTrain(Train train) {
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(train);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public long addCarriage(Carriage carriage) {
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(carriage);
    }
}
