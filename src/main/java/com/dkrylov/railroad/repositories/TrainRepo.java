package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.Carriage;
import com.dkrylov.railroad.domain.Train;

import java.util.List;

public interface TrainRepo {
    Train findTrainById(long id);
    List<Carriage> findCarriagesByTrainId(long trainId);
    long findTrainIdByCarriageId(long carriageId);
    Carriage findCarriageById(long carriageId);
    long addTrain(Train train);
    long addCarriage(Carriage carriage);
}
