package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.exceptions.DatabaseConfigurationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public class UserRepoImpl implements UserRepo {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public long addUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(user);
    }

    @Transactional(propagation = Propagation.MANDATORY, readOnly = true)
    @Override
    public User findUserById(long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(User.class, id);
    }

    @Transactional(propagation = Propagation.MANDATORY, readOnly = true)
    @Override
    public User findUserByLogin(String login) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("FROM User u WHERE u.login = :login");
        query.setParameter("login", login);
        List<User> users = query.getResultList();
        switch (users.size()) {
            case 0:
                return null;
            case 1:
                return users.get(0);
            default:
                throw new DatabaseConfigurationException("More then one user have same login");
        }
    }
}
