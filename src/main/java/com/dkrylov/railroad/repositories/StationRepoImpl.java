package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.Station;
import com.dkrylov.railroad.exceptions.DatabaseConfigurationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public class StationRepoImpl implements StationRepo {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public long addStation(Station station) {
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(station);
    }

    @Transactional(propagation = Propagation.MANDATORY, readOnly = true)
    @Override
    public Station findStationById(long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Station.class, id);
    }

    @Transactional(propagation = Propagation.MANDATORY, readOnly = true)
    @Override
    public List<Station> findAllStations() {
        Session session = sessionFactory.getCurrentSession();
        Query<Station> query = session.createQuery("FROM Station");
        return query.getResultList();
    }

    @Transactional(propagation = Propagation.MANDATORY, readOnly = true)
    @Override
    public Station findStationByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Station> query = session.createQuery("FROM Station s WHERE s.name = :name");
        query.setParameter("name", name);
        List<Station> stations = query.getResultList();
        switch (stations.size()) {
            case 0:
                return null;
            case 1:
                return stations.get(0);
            default:
                throw new DatabaseConfigurationException("More then one station has same number");
        }
    }
}
