package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.domain.PricePart;
import com.dkrylov.railroad.exceptions.DatabaseConfigurationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;


@Repository
public class PriceRepoImpl implements PriceRepo {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public double findPriceByTrainIdCarriageTypeStartStationIdEndStationId(long trainId, CarriageType carriageType,
                                                                        long startStationId, long endStationId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Double> query = session.createQuery("SELECT p.price FROM PricePart p " +
                "WHERE p.train.id = :trainId AND p.carriageType = :carriageType " +
                "AND p.startStation.id = :startStationId AND p.endStation.id = :endStationId");
        query.setParameter("trainId", trainId);
        query.setParameter("carriageType", carriageType);
        query.setParameter("startStationId", startStationId);
        query.setParameter("endStationId", endStationId);
        List<Double> list = query.list();
        if(list.size() == 1) {
            return list.get(0);
        }
        throw new DatabaseConfigurationException(
                "more than one value of ticketPrice for same train, carriageType and stations");
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public long addPricePart(PricePart pricePart) {
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(pricePart);
    }
}
