package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.User;

public interface UserRepo {
    long addUser(User user);
    User findUserById(long id);
    User findUserByLogin(String login);
}
