package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.Ticket;
import com.dkrylov.railroad.dto.admin.UserForAdminRequestDto;

import java.util.List;

public interface TicketRepo {
    List<Ticket> findTicketsByCarriageIdAndPlaceNumber(long carriageId, int placeNumber);
    List<Ticket> findTicketsByCarriageIdAndUserId(long carriageId, long userId);
    long addTemporaryTicket(Ticket ticket);
    void deleteTrashTickets(long carriageId);
    void upgradeTemporaryTicket(Ticket ticket);
    void deleteTemporaryTicketsForCurrentUser(long userId);
    boolean findTicketByIdForUpgrade(long id);
    List<UserForAdminRequestDto> findTicketsByCarriageIds(List<Long> carriageIds);
}
