package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.Ticket;
import com.dkrylov.railroad.dto.admin.UserForAdminRequestDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TicketRepoImpl implements TicketRepo {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public List<Ticket> findTicketsByCarriageIdAndPlaceNumber(long carriageId, int placeNumber) {
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("FROM Ticket t WHERE t.carriage.id = :carriageId " +
                "AND t.placeNumber = :placeNumber");
        query.setParameter("carriageId", carriageId);
        query.setParameter("placeNumber", placeNumber);
        return query.list();
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public List<Ticket> findTicketsByCarriageIdAndUserId(long carriageId, long userId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("FROM Ticket t WHERE t.carriage.id = :carriageId " +
                "AND t.user.id = :userId");
        query.setParameter("carriageId", carriageId);
        query.setParameter("userId", userId);
        return query.list();
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public long addTemporaryTicket(Ticket ticket) {
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(ticket);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public void deleteTrashTickets(long carriageId) {
        Session session = sessionFactory.getCurrentSession();
        LocalDateTime validTime = LocalDateTime.now().minusMinutes(10);
        Query<Ticket> query = session.createQuery("DELETE Ticket t WHERE t.carriage.id = :carriageId " +
                "AND t.creationTime < :validTime AND t.price < 0");
        query.setParameter("carriageId", carriageId);
        query.setParameter("validTime", validTime);
        int i = query.executeUpdate();
        System.out.println(i);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public void upgradeTemporaryTicket(Ticket ticket) {
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("UPDATE Ticket t SET t.startStation = :startStation, " +
                "t.finishStation = :finishStation, t.price = :price, t.creationTime = :creationTime " +
                "WHERE t.id = :id");
        query.setParameter("startStation", ticket.getStartStation());
        query.setParameter("finishStation", ticket.getFinishStation());
        query.setParameter("price", ticket.getPrice());
        query.setParameter("creationTime", LocalDateTime.now());
        query.setParameter("id", ticket.getId());
        query.executeUpdate();
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public void deleteTemporaryTicketsForCurrentUser(long userId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("DELETE Ticket t WHERE t.user.id = :userId " +
                "AND t.price < 0");
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public boolean findTicketByIdForUpgrade(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("FROM Ticket t WHERE t.id = :id");
        query.setParameter("id", id);
        query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
        List<Ticket> resultList = query.list();
        return resultList.size() == 1;
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public List<UserForAdminRequestDto> findTicketsByCarriageIds(List<Long> carriageIds) {
        List<UserForAdminRequestDto> result = new ArrayList<>();
        Session session = sessionFactory.getCurrentSession();
        for(long carriageId : carriageIds){
            Query<UserForAdminRequestDto> query = session.createQuery(
                    "SELECT new com.dkrylov.railroad.dto.admin.UserForAdminRequestDto" +
                            "(u.login, u.firstName, u.lastName, u.dateOfBirth," +
                            " t.placeNumber, t.startStation.name, t.finishStation.name, t.price, c.number)" +
                            " FROM Ticket t INNER JOIN User u ON u.id = t.user.id INNER JOIN Carriage c ON c.id = t.carriage.id" +
                            " WHERE t.carriage.id = :carriageId");
            query.setParameter("carriageId", carriageId);
            result.addAll(query.list());
        }
        return result;
    }
}
