package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.RouteNode;
import com.dkrylov.railroad.domain.Station;
import com.dkrylov.railroad.domain.Train;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface RouteNodeRepo {
    List<RouteNode> findRouteNodesByStationAndDate(Station station, LocalDate date);
    List<Train> findTrainsByStartStationEndStationAndStartPeriod(Station startStation, Station endStation,
                                                                 LocalDateTime startPeriod, LocalDateTime endPeriod);
    RouteNode findRouteNodeByTrainAndStation(Train train, Station station);
    List<Long> findSortedStationIdsByTrainId(long trainId);
    RouteNode findDepartureTimeByStationIdAndTrainId(long stationId, long trainId);
    long addRouteNode(RouteNode routeNode);
}
