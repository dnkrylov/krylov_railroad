package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.RouteNode;
import com.dkrylov.railroad.domain.Station;
import com.dkrylov.railroad.domain.Train;
import com.dkrylov.railroad.exceptions.DatabaseConfigurationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RouteNodeRepoImpl implements RouteNodeRepo {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public List<RouteNode> findRouteNodesByStationAndDate(Station station, LocalDate date) {
        Session session = sessionFactory.getCurrentSession();

        Query<RouteNode> query = session.createQuery("FROM RouteNode r WHERE r.station = :station " +
                "AND r.arrivalTime BETWEEN :startDate AND :endDate");
        query.setParameter("station", station);
        query.setParameter("startDate", date.atStartOfDay());
        query.setParameter("endDate", date.plusDays(1).atStartOfDay());
        return query.getResultList();
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public List<Train> findTrainsByStartStationEndStationAndStartPeriod(Station startStation, Station endStation,
                                                                        LocalDateTime startPeriod, LocalDateTime endPeriod) {
        Session session = sessionFactory.getCurrentSession();
        Query<Train> query = session.createQuery("SELECT r.train FROM RouteNode r WHERE r.station = :startStation" +
                " AND r.departureTime BETWEEN  :startPeriod AND :endPeriod" +
                " AND r.train IN (SELECT r1.train FROM RouteNode r1 WHERE r1.station = :endStation)");
        query.setParameter("startStation", startStation);
        query.setParameter("endStation", endStation);
        query.setParameter("startPeriod", startPeriod);
        query.setParameter("endPeriod", endPeriod);
        return query.getResultList();
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public RouteNode findRouteNodeByTrainAndStation(Train train, Station station) {
        Session session = sessionFactory.getCurrentSession();
        Query<RouteNode> query = session.createQuery("FROM RouteNode r WHERE r.station = :station " +
                "AND r.train = :train ");
        query.setParameter("station", station);
        query.setParameter("train", train);
        List<RouteNode> list = query.getResultList();
        if (list.size() != 1) {
            throw new DatabaseConfigurationException("one train arrives in one station mor than one time");
        }
        return list.get(0);
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public List<Long> findSortedStationIdsByTrainId(long trainId) {
        Session session = sessionFactory.getCurrentSession();
        Query<RouteNode> query = session.createQuery("FROM RouteNode r " +
                "WHERE r.train.id = :trainId ORDER BY r.departureTime");
        query.setParameter("trainId", trainId);
        return query.getResultList().stream().map(r -> r.getStation().getId()).collect(Collectors.toList());
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    @Override
    public RouteNode findDepartureTimeByStationIdAndTrainId(long stationId, long trainId) {
        Session session = sessionFactory.getCurrentSession();
        Query<RouteNode> query = session.createQuery("FROM RouteNode r " +
                "WHERE r.train.id = :trainId AND r.station.id = :stationId");
        query.setParameter("trainId", trainId);
        query.setParameter("stationId", stationId);
        List<RouteNode> list = query.list();
        switch (list.size()) {
            case 0:
                throw new DatabaseConfigurationException("train, departing from this station not found");
            case 1:
                return list.get(0);
            default:
                throw new DatabaseConfigurationException("train, departing from this station more than one time");
        }
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public long addRouteNode(RouteNode routeNode) {
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(routeNode);
    }
}
