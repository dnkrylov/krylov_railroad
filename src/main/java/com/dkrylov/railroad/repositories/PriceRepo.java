package com.dkrylov.railroad.repositories;

import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.domain.PricePart;

public interface PriceRepo {
    double findPriceByTrainIdCarriageTypeStartStationIdEndStationId(long trainId, CarriageType carriageType,
                                                                 long startStationId, long endStationId);
    long addPricePart(PricePart pricePart);
}
