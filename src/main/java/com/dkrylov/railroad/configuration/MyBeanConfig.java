package com.dkrylov.railroad.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScans(value = {@ComponentScan("com.dkrylov.railroad.controllers"),
        @ComponentScan("com.dkrylov.railroad.repositories"),
        @ComponentScan("com.dkrylov.railroad.services")})
public class MyBeanConfig {
}
