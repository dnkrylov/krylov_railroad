package com.dkrylov.railroad.dto.userdtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class TicketInfo implements Serializable {
    private int placeNumber;
    private long carriageId;
    private long userId;
    private long startStationId;
    private long endStationId;
    private long trainId;
    private long ticketId;
    private double ticketPrice;
    private String carriageType;
}
