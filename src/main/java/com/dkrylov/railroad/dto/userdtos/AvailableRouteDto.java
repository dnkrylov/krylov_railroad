package com.dkrylov.railroad.dto.userdtos;

import lombok.Data;

@Data
public class AvailableRouteDto {
    String startTime;
    String trainNumber;
    Long trainId;
    String endTime;
    Boolean canBuyTicket;
}
