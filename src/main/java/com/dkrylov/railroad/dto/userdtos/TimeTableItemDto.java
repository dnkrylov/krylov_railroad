package com.dkrylov.railroad.dto.userdtos;

import lombok.Data;

@Data
public class TimeTableItemDto {
    private String arrivalTime;
    private String trainNumber;
    private String departureTime;
}
