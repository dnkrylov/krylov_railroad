package com.dkrylov.railroad.dto.userdtos;

import lombok.Data;

import java.util.List;

@Data
public class CarriageDto {
    private Long carriageId;
    private String number;
    private String type;
    private int capacity;
    private int availablePlaces;
    private List<Integer> freePlaceNumbers;
    private double ticketPrice;
}
