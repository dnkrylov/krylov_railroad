package com.dkrylov.railroad.dto.userdtos;

import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.domain.UserRole;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.format.DateTimeFormatter;

@Data
public class UserDto {
    private Long userId;
    @NotBlank(message = "login is required")
    private String firstName;
    @NotBlank(message = "surname is required")
    private String lastName;
    @NotBlank(message = "date of birth is required")
    @Pattern(regexp="^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$",
            message="date of birth should be formatted dd.mm.yyyy")
    private String dateOfBirth;
    @NotBlank(message = "login is required")
    private String login;
    @NotBlank(message = "password is required")
    private String password;
    private UserRole userRole;

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public UserDto() {
    }

    public UserDto(String firstName, String lastName, String dateOfBirth, String name, String password, UserRole userRole) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.login = name;
        this.password = password;
        this.userRole = userRole;
    }
    public UserDto(User user) {
        this.setUserId(user.getId());
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.dateOfBirth = user.getDateOfBirth().format(dtf);
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.userRole = user.getUserRole();
    }
}
