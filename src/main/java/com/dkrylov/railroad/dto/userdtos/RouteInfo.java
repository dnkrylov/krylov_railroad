package com.dkrylov.railroad.dto.userdtos;

import lombok.Data;

@Data
public class RouteInfo {
    String startStationName;
    String departureTime;
    String endStationName;
    String arrivingTime;
    String trainNumber;
    String carriageNumber;
    String carriageType;
    String placeNumber;
    String userFirstName;
    String userLastName;
    String userDateOfBirth;
    String ticketPrice;
}
