package com.dkrylov.railroad.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RouteCreationDto {
    private String stationName;
    private String arrivingTime;
    private String departureTime;
    private String economyPrice;
    private String comfortPrice;
    private String businessPrice;
}
