package com.dkrylov.railroad.dto.admin;

import lombok.Data;

import java.util.List;

@Data
public class TrainCreationDto {
    private String number;
    List<RouteCreationDto> stationList;
    List<CarriageForTrainCreationDto> carriages;
}
