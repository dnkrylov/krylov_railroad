package com.dkrylov.railroad.dto.admin;

import lombok.Data;

@Data
public class BusinessResponse {
    private String message;
}
