package com.dkrylov.railroad.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CarriageForTrainCreationDto {
    private String carName;
    private String carType;
    private String carCapacity;
}
