package com.dkrylov.railroad.dto.admin;

import com.dkrylov.railroad.domain.Station;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Data
@RequiredArgsConstructor
public class UserForAdminRequestDto {
    private String name;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String carriageNumber;
    private Integer placeNumber;
    private String startStationName;
    private String endStationName;
    private Double price;


    public UserForAdminRequestDto(String name, String firstName, String lastName, LocalDate dateOfBirth,
                                  Integer placeNumber, String startStationName, String endStationName,
                                  Double price, String carriageNumber) {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.placeNumber = placeNumber;
        this.startStationName = startStationName;
        this.endStationName = endStationName;
        this.price = price;
        this.carriageNumber = carriageNumber;
    }
}
