package com.dkrylov.railroad.dto.admin;

import lombok.Data;

@Data
public class StationDto {
    private String name;
}
