package com.dkrylov.railroad.dto.admin;

import lombok.Data;

@Data
public class AdminTimeTableItemDto {
    private String arrivalTime;
    private String trainNumber;
    private String departureTime;
    private Long trainId;
}
