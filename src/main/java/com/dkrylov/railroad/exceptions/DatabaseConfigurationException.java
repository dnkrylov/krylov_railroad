package com.dkrylov.railroad.exceptions;

public class DatabaseConfigurationException extends RuntimeException {
    public DatabaseConfigurationException(String message) {
        super(message);
    }
}
