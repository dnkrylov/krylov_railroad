package com.dkrylov.railroad.domain;

import lombok.Data;
import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "js_train")
public class Train {

    @Id
    @Column(name = "train_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "train_name")
    private String name;
    @OneToMany(mappedBy = "train")
    private List<Carriage> carriages;
    @OneToMany(mappedBy = "train")
    private List<RouteNode> routeNodes;

    @Override
    public String toString() {
        return "Train{" +
                "id=" + id +
                ", number='" + name + '\'' +
                '}';
    }
}
