package com.dkrylov.railroad.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Entity
@Table(name = "js_ticket")
@AllArgsConstructor
@NoArgsConstructor
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ticket_id")
    private Long id;
    @Column(name = "place_number")
    private Integer placeNumber;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "carriage_id")
    private Carriage carriage;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "start_station_id")
    private Station startStation;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "finish_station_id")
    private Station finishStation;
    @Column(name = "ticket_price")
    private Double price;
    @Column(name = "creation_time")
    private LocalDateTime creationTime;
}
