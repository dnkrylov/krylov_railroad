package com.dkrylov.railroad.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "js_carriage")
@AllArgsConstructor
@NoArgsConstructor
public class Carriage {
    @Id
    @Column(name = "carriage_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "carriage_number")
    private String number;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "train_id")
    private Train train;
    @Column(name = "carriage_type")
    @Enumerated(EnumType.STRING)
    private CarriageType carriageType;
    @Column(name = "carriage_capacity")
    private Integer capacity;

    @Override
    public String toString() {
        return "Carriage{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", carriageType=" + carriageType +
                ", capacity=" + capacity +
                '}';
    }
}
