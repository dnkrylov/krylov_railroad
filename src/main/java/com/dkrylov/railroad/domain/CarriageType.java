package com.dkrylov.railroad.domain;

public enum CarriageType {
    BUSINESS, COMFORT, ECONOMY
}
