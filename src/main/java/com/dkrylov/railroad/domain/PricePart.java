package com.dkrylov.railroad.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "js_price")
public class PricePart {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "price_part_id")
    private Long id;
    @Column(name = "carriage_type")
    @Enumerated(EnumType.STRING)
    private CarriageType carriageType;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "train_id")
    private Train train;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "start_station_id")
    private Station startStation;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "finish_station_id")
    private Station endStation;
    @Column(name = "price")
    private Double price;
}
