package com.dkrylov.railroad.services;

import com.dkrylov.railroad.dto.userdtos.UserDto;

public interface RegistrationService {
    boolean isUserLoginFree(String name);
    void addUser(UserDto userDto);
}
