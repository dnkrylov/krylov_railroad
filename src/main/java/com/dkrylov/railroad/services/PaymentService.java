package com.dkrylov.railroad.services;

import com.dkrylov.railroad.dto.userdtos.RouteInfo;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;

public interface PaymentService {
    String upgradeTemporaryTicket(TicketInfo ticketInfo);
    String validateCardParameters(String cardNumber, String cardExp, String cardCVV);
    RouteInfo fillRouteInfo(TicketInfo ticketInfo);
}
