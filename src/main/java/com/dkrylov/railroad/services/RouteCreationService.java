package com.dkrylov.railroad.services;

import com.dkrylov.railroad.dto.userdtos.AvailableRouteDto;

import java.util.List;

public interface RouteCreationService {
    List<AvailableRouteDto> findAvailableRoutes(String startStationName, String endStationName, String startPeriod, String endPeriod);
}
