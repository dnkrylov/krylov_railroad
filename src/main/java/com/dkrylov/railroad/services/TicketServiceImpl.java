package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.Ticket;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.repositories.StationRepo;
import com.dkrylov.railroad.repositories.TicketRepo;
import com.dkrylov.railroad.repositories.TrainRepo;
import com.dkrylov.railroad.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class TicketServiceImpl implements TicketService{

    @Autowired
    private TicketRepo ticketRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TrainRepo trainRepo;

    @Autowired
    private StationRepo stationRepo;

    //unit
    @Transactional
    @Override
    public long createTemporaryTicket(TicketInfo ticketInfo) {
        Ticket ticket = new Ticket();
        ticket.setUser(userRepo.findUserById(ticketInfo.getUserId()));
        ticket.setCarriage(trainRepo.findCarriageById(ticketInfo.getCarriageId()));
        ticket.setPlaceNumber(ticketInfo.getPlaceNumber());
        ticket.setPrice(-1.00);
        ticket.setCreationTime(LocalDateTime.now());
        return ticketRepo.addTemporaryTicket(ticket);
    }

    @Transactional
    @Override
    public void deleteTrashTickets(long carriageId) {
        ticketRepo.deleteTrashTickets(carriageId);
    }

    //unit
    @Transactional
    @Override
    public boolean upgradeTemporaryTicket(TicketInfo ticketInfo) {
        Ticket ticket = new Ticket();
        ticket.setStartStation(stationRepo.findStationById(ticketInfo.getStartStationId()));
        ticket.setFinishStation(stationRepo.findStationById(ticketInfo.getEndStationId()));
        ticket.setPrice(ticketInfo.getTicketPrice());
        ticket.setId(ticketInfo.getTicketId());
        if(!ticketRepo.findTicketByIdForUpgrade(ticketInfo.getTicketId())){
            return false;
        }
        ticketRepo.upgradeTemporaryTicket(ticket);
        return true;
    }

    @Transactional
    @Override
    public void deleteTemporaryTicketsForCurrentUser(long userId) {
        ticketRepo.deleteTemporaryTicketsForCurrentUser(userId);
    }
}
