package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.CarriageType;

public interface PriceCalculationService {
    double getPrice(long trainId, CarriageType carriageType, long startStationId, long EndStationId);
}
