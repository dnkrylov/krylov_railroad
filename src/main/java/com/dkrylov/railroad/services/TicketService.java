package com.dkrylov.railroad.services;

import com.dkrylov.railroad.dto.userdtos.TicketInfo;

public interface TicketService {
    long createTemporaryTicket(TicketInfo ticketInfo);
    void deleteTrashTickets(long carriageId);
    boolean upgradeTemporaryTicket(TicketInfo ticketInfo);
    void deleteTemporaryTicketsForCurrentUser(long userId);
}
