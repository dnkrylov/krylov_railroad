package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.Carriage;
import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.domain.Station;
import com.dkrylov.railroad.domain.Ticket;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.dto.admin.CarriageForTrainCreationDto;
import com.dkrylov.railroad.dto.admin.RouteCreationDto;
import com.dkrylov.railroad.dto.admin.TrainCreationDto;
import com.dkrylov.railroad.exceptions.InvalidRequestParamException;
import com.dkrylov.railroad.repositories.RouteNodeRepo;
import com.dkrylov.railroad.repositories.StationRepo;
import com.dkrylov.railroad.repositories.TicketRepo;
import com.dkrylov.railroad.repositories.TrainRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class ValidatorImpl implements Validator {
    private static final String TIME_FORMAT_REGEXP =
            "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])" +
                    "([:])(0[0-9]|1[0-9]|2[0-3])([.])([0-5][0-9])$";
    private static final String PRICE_FORMAT_REGEXP = "^[0-9]{0,8}+([.][0-9]{2})?$";
    private static final String CAPACITY_FORMAT_REGEXP = "^([1-9][0-9]{0,2})$";
    private static final String CREDIT_CARD_NUMBER_REGEXP = "^[0-9]{16}";
    private static final String CREDIT_CARD_EXPIRATION_REGEXP = "^(0[1-9]|1[0-2])([\\/])([0-9][0-9])$";
    private static final String CREDIT_CARD_CVV_REGEXP = "^([0-9][0-9][0-9])$";
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy:kk.mm");

    @Autowired
    private TicketRepo ticketRepo;
    @Autowired
    private RouteNodeRepo routeNodeRepo;
    @Autowired
    private TrainRepo trainRepo;
    @Autowired
    private StationRepo stationRepo;

    //allUnit
    @Transactional(readOnly = true)
    @Override
    public boolean isPlaceFree(long carriageId, int placeNumber, long startStationId, long endStationId) {
        List<Ticket> tickets = ticketRepo.findTicketsByCarriageIdAndPlaceNumber(carriageId, placeNumber);
        if (tickets.isEmpty()) {
            return true;
        }
        List<Long> sortedStationIdList = routeNodeRepo.findSortedStationIdsByTrainId(
                trainRepo.findTrainIdByCarriageId(carriageId));
        for (Ticket ticket : tickets) {
            if (ticket.getPrice() < 0) {
                return false;
            }
            int ticketStartIndex = sortedStationIdList.indexOf(ticket.getStartStation().getId());
            int ticketEndIndex = sortedStationIdList.indexOf(ticket.getFinishStation().getId());
            int requiredStartIndex = sortedStationIdList.indexOf(startStationId);
            int requiredEndIndex = sortedStationIdList.indexOf(endStationId);
            if (ticketStartIndex < requiredEndIndex && ticketEndIndex > requiredStartIndex) {
                return false;
            }
        }
        return true;
    }

    //checking that train not departed or departing more than in 10 minutes
    @Transactional(readOnly = true)
    @Override
    public boolean validateTime(TicketInfo ticketInfo) {
        LocalDateTime departureTime = routeNodeRepo
                .findDepartureTimeByStationIdAndTrainId(ticketInfo.getStartStationId(), ticketInfo.getTrainId())
                .getDepartureTime();
        LocalDateTime now = LocalDateTime.now();
        if (now.plusMinutes(10).isAfter(departureTime)) {
            return false;
        }
        return true;
    }

    //checking that same user have no tickets for this train
    @Transactional(readOnly = true)
    @Override
    public boolean validateTicket(TicketInfo ticketInfo) {
        List<Carriage> carriages = trainRepo.findCarriagesByTrainId(ticketInfo.getTrainId());
        for (Carriage carriage : carriages) {
            List<Ticket> tickets = ticketRepo
                    .findTicketsByCarriageIdAndUserId(carriage.getId(), ticketInfo.getUserId());
            if (tickets.size() > 1) {
                return false;
            }
            if(tickets.size() == 1 && tickets.get(0).getPrice()>0) {
                return false;
            }
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isStationWithSuchNameExists(String stationName) {
        Station sameNameStation = stationRepo.findStationByName(stationName.toUpperCase());
        if (sameNameStation == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean validateReceivedTrainValues(TrainCreationDto trainCreationDto) {
        List<String> carriageTypes = new ArrayList<>();
        for (CarriageType type : CarriageType.values()) {
            carriageTypes.add(type.toString());
        }
        if (trainCreationDto.getNumber() == null || trainCreationDto.getNumber().equals("")) {
            throw new InvalidRequestParamException("train number should not be empty");
        }
        if (trainCreationDto.getCarriages() == null) {
            throw new InvalidRequestParamException("train should have at least 1 carriage");
        }
        if (trainCreationDto.getStationList() == null) {
            throw new InvalidRequestParamException("train should have at least 2 stations");
        }
        for (CarriageForTrainCreationDto car : trainCreationDto.getCarriages()) {
            if (car.getCarName() == null || car.getCarName().equals("")) {
                throw new InvalidRequestParamException("carriage number should not be empty");
            }
            if (car.getCarCapacity() == null) {
                throw new InvalidRequestParamException("carriage " + car.getCarName() + ": capacity should not be empty");
            } else if (!car.getCarCapacity().matches(CAPACITY_FORMAT_REGEXP)) {
                throw new InvalidRequestParamException("carriage " + car.getCarName() + ": invalid capacity value. " +
                        "Should be from 1 to 999");
            }
            if (car.getCarType() == null) {
                throw new InvalidRequestParamException("carriage " + car.getCarName() + ": type should not be empty");
            } else if (!carriageTypes.contains(car.getCarType())) {
                throw new InvalidRequestParamException("carriage " + car.getCarName() + ": invalid type value");
            }
        }

        for (RouteCreationDto station : trainCreationDto.getStationList()) {
            String stationName = station.getStationName();
            if (stationName == null || stationName.equals("")) {
                throw new InvalidRequestParamException("station name should not be empty");
            }
            if (station.getArrivingTime() == null || station.getDepartureTime() == null) {
                throw new InvalidRequestParamException(stationName + ": arrival/departure should not be empty");
            } else if (!station.getArrivingTime().matches(TIME_FORMAT_REGEXP) ||
                    !station.getDepartureTime().matches(TIME_FORMAT_REGEXP)) {
                throw new InvalidRequestParamException(stationName +
                        ": invalid arrival/departure time format");
            }
            if (!station.getArrivingTime().matches(TIME_FORMAT_REGEXP) ||
                    !station.getDepartureTime().matches(TIME_FORMAT_REGEXP)) {
                throw new InvalidRequestParamException(stationName +
                        ": invalid arrival/departure time format");
            }
            if (station.getEconomyPrice() == null || station.getComfortPrice() == null ||
                    station.getBusinessPrice() == null) {
                throw new InvalidRequestParamException(stationName + ": price should not be empty");
            } else if (!station.getEconomyPrice().matches(PRICE_FORMAT_REGEXP) ||
                    !station.getComfortPrice().matches(PRICE_FORMAT_REGEXP) ||
                    !station.getBusinessPrice().matches(PRICE_FORMAT_REGEXP)) {
                throw new InvalidRequestParamException(stationName +
                        ": invalid price format");
            }
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public String validateTrainParameters(TrainCreationDto trainCreationDto) {
        List<String> stationNames = new ArrayList<>();
        List<String> carriageNames = new ArrayList<>();
        List<String> departureTimes = new ArrayList<>();
        List<String> arrivingTimes = new ArrayList<>();
        if(trainCreationDto.getCarriages() == null || trainCreationDto.getCarriages().size() < 1) {
            return "train should have at least one carriage";
        }
        for(CarriageForTrainCreationDto carriage : trainCreationDto.getCarriages()) {
            if(carriageNames.contains(carriage.getCarName())) {
                return "duplicate carriage numbers: " + carriage.getCarName();
            }
            carriageNames.add(carriage.getCarName());
        }

        if(trainCreationDto.getStationList() == null || trainCreationDto.getStationList().size() < 2) {
            return "train should have at least two stations";
        }

        for(RouteCreationDto station : trainCreationDto.getStationList()) {
            if(stationNames.contains(station.getStationName())) {
                return "duplicate stations: " + station.getStationName();
            }
            if(departureTimes.contains(station.getDepartureTime())) {
                return "duplicate departure time: " + station.getDepartureTime();
            }
            if(arrivingTimes.contains(station.getArrivingTime())) {
                return "duplicate arriving time: " + station.getArrivingTime();
            }
            LocalDateTime arrTime = LocalDateTime.parse(station.getArrivingTime(), dtf);
            LocalDateTime depTime = LocalDateTime.parse(station.getDepartureTime(), dtf);
            if(depTime.isBefore(arrTime)) {
                return station.getStationName() + ": departing before arriving";
            }
            if(stationRepo.findStationByName(station.getStationName()) == null) {
                return station.getStationName() + ": station does not exist";

            }
            stationNames.add(station.getStationName());
            arrivingTimes.add(station.getArrivingTime());
            departureTimes.add(station.getDepartureTime());
        }
        return "CORRECT";
    }

    @Override
    public String validateCardParameters(String cardNumber, String cardExp, String cardCVV) {
        if(!cardNumber.matches(CREDIT_CARD_NUMBER_REGEXP)) {
            return "Invalid credit card number";
        }
        if(!cardExp.matches(CREDIT_CARD_EXPIRATION_REGEXP)) {
            return "Invalid credit card expiration";
        }
        if(!cardCVV.matches(CREDIT_CARD_CVV_REGEXP)) {
            return "Invalid credit card cvv";
        }
        return "CORRECT";
    }
}
