package com.dkrylov.railroad.services;

import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.dto.admin.TrainCreationDto;

public interface Validator {
    boolean isPlaceFree(long carriageId, int placeNumber, long startStationId, long endStationId);
    boolean validateTicket(TicketInfo ticketInfo);
    boolean validateTime(TicketInfo ticketInfo);
    boolean isStationWithSuchNameExists(String stationName);
    boolean validateReceivedTrainValues(TrainCreationDto trainCreationDto);
    String validateTrainParameters(TrainCreationDto trainCreationDto);
    String validateCardParameters(String cardNumber, String cardExp, String cardCVV);
}
