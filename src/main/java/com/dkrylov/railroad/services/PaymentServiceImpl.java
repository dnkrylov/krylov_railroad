package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.Station;
import com.dkrylov.railroad.domain.Train;
import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.dto.userdtos.RouteInfo;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.repositories.RouteNodeRepo;
import com.dkrylov.railroad.repositories.StationRepo;
import com.dkrylov.railroad.repositories.TrainRepo;
import com.dkrylov.railroad.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class PaymentServiceImpl implements PaymentService{
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy kk:mm");
    @Autowired
    private Validator validator;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private StationRepo stationRepo;

    @Autowired
    private RouteNodeRepo routeNodeRepo;

    @Autowired
    private TrainRepo trainRepo;

    @Autowired
    private UserRepo userRepo;

    @Transactional
    @Override
    public String upgradeTemporaryTicket(TicketInfo ticketInfo) {
        if (!validator.validateTime(ticketInfo)) {
            return "DEPARTED";
        }
        if (!validator.validateTicket(ticketInfo)) {
            return "DUPLICATE";
        }
        if(ticketService.upgradeTemporaryTicket(ticketInfo)){
            return "SUCCESS";
        } else {
            return "LATE";
        }
    }

    @Override
    public String validateCardParameters(String cardNumber, String cardExp, String cardCVV) {
        return validator.validateCardParameters(cardNumber, cardExp, cardCVV);
    }

    @Transactional(readOnly = true)
    @Override
    public RouteInfo fillRouteInfo(TicketInfo ticketInfo) {
        RouteInfo routeInfo = new RouteInfo();

        Station startStation = stationRepo.findStationById(ticketInfo.getStartStationId());
        routeInfo.setStartStationName(startStation.getName());
        Station endStation = stationRepo.findStationById(ticketInfo.getEndStationId());
        routeInfo.setEndStationName(endStation.getName());

        Train train = trainRepo.findTrainById(ticketInfo.getTrainId());
        routeInfo.setTrainNumber(train.getName());
        routeInfo.setCarriageType(ticketInfo.getCarriageType());
        routeInfo.setCarriageNumber(trainRepo.findCarriageById(ticketInfo.getCarriageId()).getNumber());
        routeInfo.setPlaceNumber("" + ticketInfo.getPlaceNumber());

        LocalDateTime departingTime = routeNodeRepo.findRouteNodeByTrainAndStation(train,startStation)
                .getDepartureTime();
        routeInfo.setDepartureTime(departingTime.format(formatter));
        LocalDateTime arrivingTimeTime = routeNodeRepo.findRouteNodeByTrainAndStation(train,endStation)
                .getArrivalTime();
        routeInfo.setArrivingTime(arrivingTimeTime.format(formatter));
        fillUser(routeInfo, ticketInfo);
        return routeInfo;
    }

    private void fillUser(RouteInfo routeInfo, TicketInfo ticketInfo) {
        User user = userRepo.findUserById(ticketInfo.getUserId());
        routeInfo.setUserFirstName(user.getFirstName());
        routeInfo.setUserLastName(user.getLastName());
        routeInfo.setUserDateOfBirth(user.getDateOfBirth().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        routeInfo.setTicketPrice("" + ticketInfo.getTicketPrice());
    }
}
