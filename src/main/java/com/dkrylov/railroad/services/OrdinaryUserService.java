package com.dkrylov.railroad.services;

import com.dkrylov.railroad.dto.userdtos.AvailableRouteDto;
import com.dkrylov.railroad.dto.userdtos.TimeTableItemDto;

import java.util.List;

public interface OrdinaryUserService {
    List<String> getStationNamesList();
    List<TimeTableItemDto> getTimeTableItems(String stationName, String date);
    List<AvailableRouteDto> findRoutes(String startStationName, String endStationName,
                                       String startPeriod, String endPeriod);
    long findStationIdByName(String stationName);
}
