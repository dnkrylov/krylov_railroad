package com.dkrylov.railroad.services;

import com.dkrylov.railroad.dto.userdtos.CarriageDto;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;

import java.util.List;

public interface PlaceSelectionService {
    List<CarriageDto> getCarriages(TicketInfo ticketInfo);
    long createTemporaryTicket(TicketInfo ticketInfo);
    double getTicketPrice(TicketInfo ticketInfo);
}
