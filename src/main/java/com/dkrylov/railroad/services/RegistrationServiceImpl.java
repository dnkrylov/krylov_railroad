package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.domain.UserRole;
import com.dkrylov.railroad.dto.userdtos.UserDto;
import com.dkrylov.railroad.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private UserRepo userRepo;

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    @Transactional(readOnly = true)
    @Override
    public boolean isUserLoginFree(String name) {
        return userRepo.findUserByLogin(name) == null;
    }

    @Transactional
    @Override
    public void addUser(UserDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setDateOfBirth(LocalDate.parse(userDto.getDateOfBirth(), dtf));
        user.setLogin(userDto.getLogin());
        user.setPassword(new BCryptPasswordEncoder().encode(userDto.getPassword()));
        user.setUserRole(UserRole.ROLE_USER);
        userRepo.addUser(user);
    }
}
