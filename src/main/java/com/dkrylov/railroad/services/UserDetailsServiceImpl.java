package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.User;
import com.dkrylov.railroad.dto.userdtos.UserDetailsDto;
import com.dkrylov.railroad.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;

    //allUnit
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String name) {
        User user = userRepo.findUserByLogin(name);
        if (user == null) {
            throw new UsernameNotFoundException("User: " + name + " not found");
        }
        return new UserDetailsDto(user);
    }
}
