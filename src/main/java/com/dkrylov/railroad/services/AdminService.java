package com.dkrylov.railroad.services;

import com.dkrylov.railroad.dto.admin.AdminTimeTableItemDto;
import com.dkrylov.railroad.dto.admin.StationDto;
import com.dkrylov.railroad.dto.admin.TrainCreationDto;
import com.dkrylov.railroad.dto.admin.UserForAdminRequestDto;

import java.util.List;

public interface AdminService {
    List<String> getStationNamesList();
    boolean addStation(StationDto stationDto);
    boolean validateReceivedValues(TrainCreationDto trainCreationDto);
    String addTrain(TrainCreationDto trainCreationDto);
    List<AdminTimeTableItemDto> getTimeTable(String stationName, String date);
    List<UserForAdminRequestDto> getPassengers(long trainId);
}
