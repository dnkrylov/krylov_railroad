package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.repositories.PriceRepo;
import com.dkrylov.railroad.repositories.RouteNodeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PriceCalculationServiceImpl implements PriceCalculationService {

    @Autowired
    private PriceRepo priceRepo;
    @Autowired
    private RouteNodeRepo routeNodeRepo;

    @Transactional(readOnly = true)
    @Override
    public double getPrice(long trainId, CarriageType carriageType, long startStationId, long endStationId) {
        List<Long> sortedStationIds = routeNodeRepo.findSortedStationIdsByTrainId(trainId);
        double price = 0;
        for(int i = sortedStationIds.indexOf(startStationId);
            i < sortedStationIds.indexOf(endStationId); i++) {
            price += priceRepo.findPriceByTrainIdCarriageTypeStartStationIdEndStationId(trainId,
                    carriageType, sortedStationIds.get(i), sortedStationIds.get(i+1));
        }
        return price;
    }
}
