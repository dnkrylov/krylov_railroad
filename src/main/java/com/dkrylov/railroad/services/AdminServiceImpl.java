package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.*;
import com.dkrylov.railroad.dto.admin.*;
import com.dkrylov.railroad.exceptions.UnknownException;
import com.dkrylov.railroad.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminServiceImpl implements AdminService {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy:kk.mm");
    @Autowired
    private StationRepo stationRepo;

    @Autowired
    private OrdinaryUserService ordinaryUserService;

    @Autowired
    private TrainRepo trainRepo;

    @Autowired
    private Validator validator;

    @Autowired
    private RouteNodeRepo routeNodeRepo;

    @Autowired
    private PriceRepo priceRepo;

    @Autowired
    private TicketRepo ticketRepo;

    @Transactional
    @Override
    public boolean addStation(StationDto stationDto) {
        if (validator.isStationWithSuchNameExists(stationDto.getName())) {
            return false;
        }
        Station newStation = new Station();
        newStation.setName(stationDto.getName().toUpperCase());
        stationRepo.addStation(newStation);
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public List<String> getStationNamesList() {
        return ordinaryUserService.getStationNamesList();
    }


    @Override
    public boolean validateReceivedValues(TrainCreationDto trainCreationDto) {
        return validator.validateReceivedTrainValues(trainCreationDto);
    }

    @Transactional
    @Override
    public String addTrain(TrainCreationDto trainCreationDto) {
        String result = validator.validateTrainParameters(trainCreationDto);
        if(!result.equals("CORRECT")){
            return result;
        }
        Train train = new Train();
        train.setName(trainCreationDto.getNumber());
        long trainId = trainRepo.addTrain(train);
        train.setId(trainId);
        List<RouteNode> sortedRoute = new ArrayList<>();
        for (RouteCreationDto station : trainCreationDto.getStationList()) {
            RouteNode routeNode = fillRouteNodeInfo(station, train);
            routeNodeRepo.addRouteNode(routeNode);
            if (sortedRoute.isEmpty()) {
                sortedRoute.add(routeNode);
            } else {
                int i = 0;
                while (sortedRoute.get(i).getArrivalTime().isBefore(routeNode.getArrivalTime())) {
                    i++;
                    if (sortedRoute.size() == i) {
                        break;
                    }
                }
                sortedRoute.add(i, routeNode);
            }
        }
        for (CarriageForTrainCreationDto dto : trainCreationDto.getCarriages()) {
            trainRepo.addCarriage(fillCarriageInfo(dto, train));
        }
        for (int i = 1; i < sortedRoute.size(); i++) {
            PricePart pricePart = new PricePart();
            pricePart.setStartStation(sortedRoute.get(i - 1).getStation());
            pricePart.setEndStation(sortedRoute.get(i).getStation());
            RouteCreationDto station = null;
            for (RouteCreationDto temp : trainCreationDto.getStationList()) {
                if (pricePart.getEndStation().getName().equals(temp.getStationName())) {
                    station = temp;
                    break;
                }
            }
            if (station == null) {
                throw new UnknownException();
            }
            priceRepo.addPricePart(fillPricePart(pricePart, station, train, CarriageType.BUSINESS));
            priceRepo.addPricePart(fillPricePart(pricePart, station, train, CarriageType.COMFORT));
            priceRepo.addPricePart(fillPricePart(pricePart, station, train, CarriageType.ECONOMY));
        }
        return "Train successfully created. Train id: " + trainId;
    }

    @Transactional(readOnly = true)
    @Override
    public List<AdminTimeTableItemDto> getTimeTable(String stationName, String date) {
        LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        List<RouteNode> routeNodeList = routeNodeRepo
                .findRouteNodesByStationAndDate(stationRepo.findStationByName(stationName), localDate);
        return createAdminTimeTableItemDto(routeNodeList);
    }

    @Transactional(readOnly = true)
    @Override
    public List<UserForAdminRequestDto> getPassengers(long trainId) {
        List<Long> carriageIds = trainRepo.findCarriagesByTrainId(trainId).stream()
                .map(carriage -> carriage.getId()).collect(Collectors.toList());
        return ticketRepo.findTicketsByCarriageIds(carriageIds);
    }

    private RouteNode fillRouteNodeInfo(RouteCreationDto station, Train train) {
        RouteNode routeNode = new RouteNode();
        routeNode.setArrivalTime(LocalDateTime.parse(station.getArrivingTime(), formatter));
        routeNode.setDepartureTime(LocalDateTime.parse(station.getDepartureTime(), formatter));
        routeNode.setStation(stationRepo.findStationByName(station.getStationName()));
        routeNode.setTrain(train);
        return routeNode;
    }

    private Carriage fillCarriageInfo(CarriageForTrainCreationDto dto, Train train) {
        Carriage carriage = new Carriage();
        carriage.setNumber(dto.getCarName());
        carriage.setCapacity(Integer.parseInt(dto.getCarCapacity()));
        carriage.setCarriageType(CarriageType.valueOf(dto.getCarType()));
        carriage.setTrain(train);
        return carriage;
    }

    private PricePart fillPricePart(PricePart pricePart, RouteCreationDto station, Train train, CarriageType carType) {
        PricePart newPricePart = new PricePart();
        newPricePart.setStartStation(pricePart.getStartStation());
        newPricePart.setPrice(Double.parseDouble(station.getBusinessPrice()));
        newPricePart.setEndStation(pricePart.getEndStation());
        newPricePart.setTrain(train);
        newPricePart.setCarriageType(carType);
        return newPricePart;
    }

    private List<AdminTimeTableItemDto> createAdminTimeTableItemDto (List<RouteNode> routeNodeList){
        return routeNodeList.stream().map(routeNode -> {
            AdminTimeTableItemDto adminTimeTableItemDto = new AdminTimeTableItemDto();
            adminTimeTableItemDto.setArrivalTime(routeNode.getArrivalTime().format(formatter));
            adminTimeTableItemDto.setDepartureTime(routeNode.getDepartureTime().format(formatter));
            adminTimeTableItemDto.setTrainNumber(routeNode.getTrain().getName());
            adminTimeTableItemDto.setTrainId(routeNode.getTrain().getId());
            return adminTimeTableItemDto;
        }).collect(Collectors.toList());
    }
}
