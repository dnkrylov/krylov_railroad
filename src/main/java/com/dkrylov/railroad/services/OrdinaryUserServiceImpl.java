package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.RouteNode;
import com.dkrylov.railroad.dto.userdtos.AvailableRouteDto;
import com.dkrylov.railroad.dto.userdtos.TimeTableItemDto;
import com.dkrylov.railroad.repositories.RouteNodeRepo;
import com.dkrylov.railroad.repositories.StationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrdinaryUserServiceImpl implements OrdinaryUserService {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy kk:mm");
    @Autowired
    private RouteNodeRepo routeNodeRepo;

    @Autowired
    private StationRepo stationRepo;

    @Autowired
    private RouteCreationService routeCreationService;

    @Transactional(readOnly = true)
    @Override
    public List<String> getStationNamesList() {
        List<String> stations = stationRepo.findAllStations().stream()
                .map(s -> s.getName()).collect(Collectors.toList());
        Collections.sort(stations);
        return stations;
    }

    @Transactional(readOnly = true)
    @Override
    public List<TimeTableItemDto> getTimeTableItems(String stationName, String date) {
        LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        List<RouteNode> routeNodeList = routeNodeRepo
                .findRouteNodesByStationAndDate(stationRepo.findStationByName(stationName), localDate);
        return getTimeTableItemDtos(routeNodeList);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AvailableRouteDto> findRoutes(String startStationName, String endStationName,
                                              String startPeriod, String endPeriod) {
        return routeCreationService.findAvailableRoutes(startStationName, endStationName, startPeriod, endPeriod);
    }

    @Transactional(readOnly = true)
    @Override
    public long findStationIdByName(String stationName) {
        return stationRepo.findStationByName(stationName).getId();
    }

    private List<TimeTableItemDto> getTimeTableItemDtos(List<RouteNode> routeNodeList) {
        return routeNodeList.stream().map(routeNode -> {
            TimeTableItemDto timeTableItemDto = new TimeTableItemDto();
            timeTableItemDto.setArrivalTime(routeNode.getArrivalTime().format(formatter));
            timeTableItemDto.setDepartureTime(routeNode.getDepartureTime().format(formatter));
            timeTableItemDto.setTrainNumber(routeNode.getTrain().getName());
            return timeTableItemDto;
        }).collect(Collectors.toList());
    }
}
