package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.Carriage;
import com.dkrylov.railroad.domain.CarriageType;
import com.dkrylov.railroad.dto.userdtos.CarriageDto;
import com.dkrylov.railroad.dto.userdtos.TicketInfo;
import com.dkrylov.railroad.repositories.TrainRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlaceSelectionServiceImpl implements PlaceSelectionService {

    @Autowired
    private Validator validator;

    @Autowired
    private TrainRepo trainRepo;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private PriceCalculationService priceCalculationService;

    @Transactional
    public List<CarriageDto> getCarriages(TicketInfo ticketInfo) {
        ticketService.deleteTemporaryTicketsForCurrentUser(ticketInfo.getUserId());
        List<Carriage> carriages = trainRepo.findCarriagesByTrainId(ticketInfo.getTrainId());
        List<CarriageDto> carriageDtos = new ArrayList<>();
        for(Carriage carriage: carriages) {
            CarriageDto carriageDto = new CarriageDto();
            carriageDto.setCarriageId(carriage.getId());
            carriageDto.setType(carriage.getCarriageType().toString());
            carriageDto.setCapacity(carriage.getCapacity());
            carriageDto.setNumber(carriage.getNumber());
            List<Integer> freePlacesNumbers = new ArrayList<>();
            int freeplaces = 0;
            ticketService.deleteTrashTickets(carriage.getId());
            for(int i = 1; i <= carriage.getCapacity(); i++) {
                if(validator.isPlaceFree(carriageDto.getCarriageId(), i,
                        ticketInfo.getStartStationId(), ticketInfo.getEndStationId())) {
                    freeplaces++;
                    freePlacesNumbers.add(i);
                }
            }
            carriageDto.setAvailablePlaces(freeplaces);
            carriageDto.setFreePlaceNumbers(freePlacesNumbers);
            carriageDto.setTicketPrice(priceCalculationService.getPrice(ticketInfo.getTrainId(),
                    carriage.getCarriageType(), ticketInfo.getStartStationId(),
                    ticketInfo.getEndStationId()));
            carriageDtos.add(carriageDto);
        }
        return carriageDtos;
    }

    @Transactional
    public long createTemporaryTicket(TicketInfo ticketInfo){
        return ticketService.createTemporaryTicket(ticketInfo);
    }

    @Transactional(readOnly = true)
    @Override
    public double getTicketPrice(TicketInfo ticketInfo) {
        return priceCalculationService.getPrice(ticketInfo.getTrainId(),
                CarriageType.valueOf(ticketInfo.getCarriageType()), ticketInfo.getStartStationId(),
                ticketInfo.getEndStationId());
    }
}
