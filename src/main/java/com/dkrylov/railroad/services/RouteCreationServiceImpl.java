package com.dkrylov.railroad.services;

import com.dkrylov.railroad.domain.RouteNode;
import com.dkrylov.railroad.domain.Station;
import com.dkrylov.railroad.domain.Train;
import com.dkrylov.railroad.dto.userdtos.AvailableRouteDto;
import com.dkrylov.railroad.repositories.RouteNodeRepo;
import com.dkrylov.railroad.repositories.StationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RouteCreationServiceImpl implements RouteCreationService {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy kk:mm");

    @Autowired
    private RouteNodeRepo routeNodeRepo;
    @Autowired
    private StationRepo stationRepo;

    //unitAll
    @Transactional(readOnly = true)
    @Override
    public List<AvailableRouteDto> findAvailableRoutes(String startStationName, String endStationName,
                                                       String startPeriod, String endPeriod) {
        Station startStation = stationRepo.findStationByName(startStationName);
        Station endStation = stationRepo.findStationByName(endStationName);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy:kk.mm");
        LocalDateTime startOfStartPeriod = LocalDateTime.parse(startPeriod, dtf);
        LocalDateTime endOfStartPeriod = LocalDateTime.parse(endPeriod, dtf);
        List<Train> trains = routeNodeRepo.findTrainsByStartStationEndStationAndStartPeriod(
                startStation,endStation,startOfStartPeriod,endOfStartPeriod);
        List<AvailableRouteDto> routes = new ArrayList<>();
        for (Train train: trains) {
            RouteNode startNode = routeNodeRepo.findRouteNodeByTrainAndStation(train, startStation);
            RouteNode endNode = routeNodeRepo.findRouteNodeByTrainAndStation(train, endStation);
            if(startNode.getDepartureTime().isBefore(endNode.getArrivalTime())) {
                routes.add(fillRouteDto(startNode, endNode));
            }
        }
        return routes;
    }

    private AvailableRouteDto fillRouteDto(RouteNode startNode, RouteNode endNode) {
        AvailableRouteDto availableRouteDto = new AvailableRouteDto();
        availableRouteDto.setStartTime(startNode.getDepartureTime().format(formatter));
        availableRouteDto.setEndTime(endNode.getArrivalTime().format(formatter));
        availableRouteDto.setTrainNumber(startNode.getTrain().getName());
        availableRouteDto.setTrainId(startNode.getTrain().getId());
        LocalDateTime canBuyTicketTime = startNode.getDepartureTime().minusMinutes(10);
        availableRouteDto.setCanBuyTicket(LocalDateTime.now().isBefore(canBuyTicketTime));
        return availableRouteDto;
    }
}
