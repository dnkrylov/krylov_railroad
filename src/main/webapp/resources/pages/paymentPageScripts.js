$(document).ready(function () {
    $('#buyTicket').click(function () {
        var cardNumber = $('#cardNumber').val();
        var cardExp = $('#cardExp').val();
        var cardCVV = $('#cardCVV').val();
        if(!validateCardNumber(cardNumber)) {
            return;
        }
        if(!validateCardExp(cardExp)) {
            return;
        }
        if(!validateCVV(cardCVV)) {
            return;
        }
        $.ajax({
            type: "POST",
            url: path + "/registered/tryToBuyTicket/",
            data: {
                cardCVV: cardCVV,
                cardNumber: cardNumber,
                cardExp: cardExp
            },
            success: function (data) {
                var successPattern = /SUCCESS/;
                if(successPattern.test(data.message)) {
                    $('.modal-title').html('SUCCESS');
                    $('.modal-body').html(data.message);
                    $('#myModal').modal('show');
                    cleanPaymentInfoColSuccess();
                } else {
                    $('.modal-title').html('ERROR');
                    $('.modal-body').html(data.message);
                    $('#myModal').modal('show');
                    cleanPaymentInfoColFail();
                }
                $('#changePlaceButton').hide();
            },
            error: function () {
                $('.modal-title').html('ERROR');
                $('.modal-body').html('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                    ' WE ARE WORKING ON THIS PROBLEM');
                $('#myModal').modal('show');
            }
        });
    });
});

function validateCardNumber(cardNumber) {
    var cardPattern = /^[0-9]{16}/;
    if (!cardPattern.test(cardNumber)) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('INVALID CREDIT CARD NUMBER');
        $('#myModal').modal('show');
        return false;
    }
    return true;
}

function validateCardExp(cardExp) {
    var expPattern = /^(0[1-9]|1[0-2])([\/])([0-9][0-9])$/;
    if (!expPattern.test(cardExp)) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('INVALID CREDIT CARD EXPIRATION');
        $('#myModal').modal('show');
        return false;
    }
    return true;
}

function validateCVV(cardCVV) {
    var cvvPattern = /^([0-9][0-9][0-9])$/;
    if (!cvvPattern.test(cardCVV)) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('INVALID CREDIT CARD CVV');
        $('#myModal').modal('show');
        return false;
    }
    return true;
}

function cleanPaymentInfoColFail() {
    $('#paymentInfoCol').empty();
    $('#paymentInfoCol').append('<img src=' + path + '/resources/images/fail.jpg>')
}

function cleanPaymentInfoColSuccess() {
    $('#paymentInfoCol').empty();
    $('#paymentInfoCol').append('<img src=' + path + '/resources/images/success.jpg>')
}