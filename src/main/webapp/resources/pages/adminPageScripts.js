var carriageTypes = {};
var stationCounter = 0;
var carriageCounter = 0;
var stations = {
    data:[],
    list: {
        match: {
            enabled:true
        }
    }
};


$(document).ready(function () {

    $('.adminForm').hide();

    $.get('getStations', function (data) {
        for (var i = 0; i < data.length; i++) {
            stations.data.push(data[i]);
        }
    });

    $('#addTrain').click(function () {
        $('#trainCreationForm').show();
        $('#passengerShowForm').hide();
        $('#stationAdditionForm').hide();
        $('.trainTable').hide();
        $('#carriageTable').empty();
        $('#stationTable').empty();
        $("#trainNumber").empty();
        stationCounter = 0;
        carriageCounter = 0;
        $.get('getCarriageTypes', function (data) {
            carriageTypes = data;
        });
    });

    $('#showPassengers').click(function () {
        $('#passengerShowForm').show();
        $('#showPassengersTableButton').hide();
        $('#trainCreationForm').hide();
        $('#stationAdditionForm').hide();
        $('#stationForTimeTable').easyAutocomplete(stations);
    });

    $('#addNewStation').click(function () {
        $('#stationAdditionForm').show();
        $('#trainCreationForm').hide();
        $('#passengerShowForm').hide();
    });
});

//Train creation functions

$(document).ready(function () {
    $('#addCarriage').click(function () {
        $('#carriageTableForm').show();
        $('#carriageTable').append('<tr id="carriageTableRow' + carriageCounter +'"></tr>');
        appendCarName();
        appendCarriageSelector();
        appendCapacity();
        carriageCounter++;
    })
});

$(document).ready(function () {
    $('#removeCarriage').click(function () {
        if(carriageCounter > 0) {
            carriageCounter--;
            $('#carriageTableRow' + carriageCounter).remove();
        }
        if(carriageCounter === 0) {
            $('#carriageTableForm').hide();
        }
    })
});

function appendCarName() {
    $('#carriageTableRow' + carriageCounter).append('<td><input type="text" class="text-center" placeholder="number" id="carriageName' + carriageCounter + '"/>');
}
function appendCarriageSelector() {
    $('#carriageTableRow' + carriageCounter).append('<td><select class="form-control" class="text-center" id="carriageType' + carriageCounter + '">');
    appendCarriageTypeOptions();
    $('#carriageTableRow' + carriageCounter).append('</select></td>');
}

function appendCarriageTypeOptions() {
    for (var i = 0; i < carriageTypes.length; i++) {
        $("#carriageType" + carriageCounter).append('<option value=' + carriageTypes[i] + '>' + carriageTypes[i] + '</option>');
    }
}

function appendCapacity() {
    $('#carriageTableRow' + carriageCounter).append('<td><input type="text" placeholder="capacity" class="text-center" id="carriageCapacity' + carriageCounter + '"/></tr>');
}

$(document).ready(function () {
    $('#addStation').click(function () {
        $('#stationTableForm').show();
        $('#stationTable').append('<tr id="stationTableRow' + stationCounter + '"></tr>');
        appendStationSelector();
        appendArrivalTime();
        appendDepartureTime();
        appendBusinessPrice();
        appendComfortPrice();
        appendEconomyPrice();
        stationCounter++;
    })
});

$(document).ready(function () {
    $('#removeStation').click(function () {
        if(stationCounter > 0) {
            stationCounter--;
            $('#stationTableRow' + stationCounter).remove();
        }
        if(stationCounter === 0) {
            $('#stationTableForm').hide();
        }
    })
});

function appendStationSelector() {
    $('#stationTableRow' + stationCounter).append('<td><input class="form-control text-center" autocomplete="off" id="selectedStation' + stationCounter + '" placeholder="start typing"/></td>');
    fillStationList(stationCounter);
}

function appendArrivalTime() {
    $('#stationTableRow' + stationCounter).append('<td><input type="text" class="text-center" placeholder="dd.mm.yyyy:hh.mm" id="arrivingTime' + stationCounter + '"</td>');
}

function appendDepartureTime() {
    $('#stationTableRow' + stationCounter).append('<td><input type="text" class="text-center" placeholder="dd.mm.yyyy:hh.mm" id="departureTime' + stationCounter + '"</td>');
}

function appendBusinessPrice() {
    if(stationCounter === 0){
        $('#stationTableRow' + stationCounter).append('<td><input type="text" class="text-center" placeholder="from previous station" disabled value="0" id="businessPrice' + stationCounter + '"</td>');
    } else {
        $('#stationTableRow' + stationCounter).append('<td><input type="text" class="text-center" placeholder="from previous station" id="businessPrice' + stationCounter + '"</td>');
    }

}

function appendComfortPrice() {
    if(stationCounter === 0) {
        $('#stationTableRow' + stationCounter).append('<td><input type="text" class="text-center" placeholder="from previous station" disabled value="0" id="comfortPrice' + stationCounter + '"</td>');
    }else {
        $('#stationTableRow' + stationCounter).append('<td><input type="text" class="text-center" placeholder="from previous station" id="comfortPrice' + stationCounter + '"</td>');

    }
}

function appendEconomyPrice() {
    if(stationCounter === 0) {
        $('#stationTableRow' + stationCounter).append('<td><input type="text" class="text-center" placeholder="from previous station" disabled value="0" id="economyPrice' + stationCounter + '"</td>');

    } else {
        $('#stationTableRow' + stationCounter).append('<td><input type="text" class="text-center" placeholder="from previous station" id="economyPrice' + stationCounter + '"</td>');

    }
}

$(document).ready(function () {
    $('#submitTrain').click(function () {
        var train = {}
        train["number"] = $("#trainNumber").val();
        train["stationList"] = [];
        train["carriages"] = [];
        for (var i = 0; i < stationCounter; i++) {
            train.stationList.push({
                "stationName": $("#selectedStation" + i).val(),
                "arrivingTime": $("#arrivingTime" + i).val(),
                "departureTime": $("#departureTime" + i).val(),
                "businessPrice": $("#businessPrice" + i).val(),
                "comfortPrice": $("#comfortPrice" + i).val(),
                "economyPrice": $("#economyPrice" + i).val()
            });
        }
        for (var i = 0; i < carriageCounter; i++) {
            train.carriages.push({
                "carName": $("#carriageName" + i).val(),
                "carType": $("#carriageType" + i).val(),
                "carCapacity": $("#carriageCapacity" + i).val()
            });
        }
        if (!validateTrainForm(train)) {
            return;
        }
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: path + "/admin/addTrain",
            data: JSON.stringify(train),
            dataType: "json",
            success: function (data) {
                var successPattern = /success/;
                if(successPattern.test(data.message)) {
                    stationCounter = 0;
                    carriageCounter = 0;
                    $('.modal-title').html('SUCCESS');
                    $('.modal-body').html(data.message);
                    $('#myModal').modal('show');
                    $('#carriageTable').empty();
                    $('#stationTable').empty();
                    $("#trainNumber").empty();
                    $('#trainCreationForm').hide();
                } else {
                    $('.modal-title').html('ERROR');
                    $('.modal-body').html(data.message);
                    $('#myModal').modal('show');
                }
            },
            error: function (data) {
                $('.modal-title').html('ERROR');
                $('.modal-body').html('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                    ' WE ARE WORKING ON THIS PROBLEM    ' + data.message);
                $('#myModal').modal('show');
            }
        })
    })
});

function validateTrainForm(train) {
    if (!$("#trainNumber").val()) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('INPUT TRAIN NUMBER');
        $('#myModal').modal('show');
        return false;
    }
    if (train.stationList.length < 2) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('ROUTE SHOULD CONTAIN AT LEAST 2 STATIONS');
        $('#myModal').modal('show');
        return false;
    }
    if (!validateTrainStations(train.stationList)) {
        return false;
    }
    if (train.carriages.length < 1) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('TRAIN SHOULD HAVE AT LEAST 1 CARRIAGE');
        $('#myModal').modal('show');
        return false;
    }
    if (!validateCarriages(train.carriages)) {
        return false;
    }
    return true;
}

function validateTrainStations(stationList) {
    var timePattern = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])([:])(0[0-9]|1[0-9]|2[0-3])([.])([0-5][0-9])$/;
    var priceFormat = /^[0-9]{0,8}([.][0-9]{2})?$/;

    for (var i = 0; i < stationList.length; i++) {
        if(!validateStation(stationList[i].stationName)){
            $('.modal-title').html('ERROR');
            $('.modal-body').html('STATION WITH NAME ' + stationList[i].stationName + '\n DOES NOT EXISTS');
            $('#myModal').modal('show');
            return false;
        }

        if (!timePattern.test(stationList[i].arrivingTime)) {
            $('.modal-title').html('ERROR');
            $('.modal-body').html("INVALID " + stationList[i].stationName + " ARRIVALTIME");
            $('#myModal').modal('show');
            return false;
        }
        if (!timePattern.test(stationList[i].departureTime)) {
            $('.modal-title').html('ERROR');
            $('.modal-body').html("INVALID " + stationList[i].stationName + " DEPARTURETIME");
            $('#myModal').modal('show');
            return false;
        }
        if (!priceFormat.test(stationList[i].businessPrice)) {
            $('.modal-title').html('ERROR');
            $('.modal-body').html("INVALID " + stationList[i].stationName + " BUSINESS PRICE \n " +
                "SELECT PRICE BETWEEN 0.00 AND 99999999.99");
            $('#myModal').modal('show');
            return false;
        }
        if (!priceFormat.test(stationList[i].comfortPrice)) {
            $('.modal-title').html('ERROR');
            $('.modal-body').html("INVALID " + stationList[i].stationName + " COMFORT PRICE  \n" +
                "SELECT PRICE BETWEEN 0.00 AND 99999999.99");
            $('#myModal').modal('show');
            return false;
        }
        if (!priceFormat.test(stationList[i].economyPrice)) {
            $('.modal-title').html('ERROR');
            $('.modal-body').html("INVALID " + stationList[i].stationName + " ECONOMY PRICE \n " +
                "SELECT PRICE BETWEEN 0.00 AND 99999999.99");
            $('#myModal').modal('show');
            return false;
        }
    }
    return true;
}

function validateCarriages(carriageList) {
    var capacityFormat = /^([1-9][0-9]{0,2})$/;
    for (var i = 0; i < carriageList.length; i++) {
        if (carriageList[i].carName === '') {
            $('.modal-title').html('ERROR');
            $('.modal-body').html("INVALID CARRIAGE " + (i + 1) + " NUMBER");
            $('#myModal').modal('show');
            return false;
        }
        if (!capacityFormat.test(carriageList[i].carCapacity)) {
            $('.modal-title').html('ERROR');
            $('.modal-body').html("INVALID CARRIAGE " + carriageList[i].carName + " CAPACITY" +
                " \n SHOULD BE BETWEEN 1 AND 999");
            $('#myModal').modal('show');
            return false;
        }
    }
    return true;

}


//adding station functions
$(document).ready(function () {
    $('#submitStation').click(function () {
        var station = {};
        station["name"] = $("#newStationName").val();
        if (!validateStationForm(station)) {
            return;
        }
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: path + "/admin/addStation",
            data: JSON.stringify(station),
            dataType: "json",
            success: function (data) {
                var successPattern = /success/;
                if(successPattern.test(data.message)) {
                    $('.modal-title').html('SUCCESS');
                    $('.modal-body').html(data.message);
                    $('#myModal').modal('show');
                    $.get('getStations', function (data) {
                        stations.data = [];
                        for (var i = 0; i < data.length; i++) {
                            stations.data.push(data[i]);
                        }
                    });
                } else {
                    $('.modal-title').html('ERROR');
                    $('.modal-body').html(data.message);
                    $('#myModal').modal('show');
                }
            },
            error: function () {
                $('.modal-title').html('ERROR');
                $('.modal-body').html('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                    ' WE ARE WORKING ON THIS PROBLEM');
                $('#myModal').modal('show');
            }
        })
    })
});

function validateStationForm(station) {
    if (station.name === '') {
        $('.modal-title').html('ERROR');
        $('.modal-body').html("INPUT STATION NAME");
        $('#myModal').modal('show');
        return false;
    }
    if(stations.data.includes(station.name)) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html("STATION WITH SAME NAME IS ALREADY EXIST");
        $('#myModal').modal('show');
        return false;
    }
    return true;
}

$(document).ready(function () {
    $('#getTrains').click(function () {
        var station = $("#stationForTimeTable").val();
        var date = $("#dateForTimeTable").val();
        if (!validateTimeTableInfo(date)) {
            return;
        }
        if(!validateStation(station)){
            $('.modal-title').html('ERROR');
            $('.modal-body').html('STATION WITH NAME ' + station + '\n DOES NOT EXISTS');
            $('#myModal').modal('show');
            return;
        }
        $.ajax({
            type: "GET",
            url: path + "/admin/getTrains",
            data: {
                station: station,
                date: date
            },
            success: function (data) {
                $('#passengerTable').empty();
                fillTimeTable(data);
            },
            error: function () {
                $('.modal-title').html('ERROR');
                $('.modal-body').html('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                    ' WE ARE WORKING ON THIS PROBLEM');
                $('#myModal').modal('show');
            }
        })
    })
});


//checking passenger functions

function validateTimeTableInfo(date) {
    var datePattern = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$/;
    if (!datePattern.test(date)) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('INVALID DATE FORMAT');
        $('#myModal').modal('show');
        return false;
    }
    return true;
}

function fillTimeTable(data) {
    $('#trainSelectionContainer').remove();
    $('#trainSelection').prepend('<div class="container text-center" id=trainSelectionContainer></div>');
    $('#trainSelectionContainer').append('<label for="availableTrains">SELECT TRAIN</label>')
    $('#trainSelectionContainer').append('<select class="form-control" id="availableTrains"></select>');
    for (var i = 0; i < data.length; i++) {
        $('#availableTrains').append(($('<option></option>'))
            .attr('value', data[i].trainId).text('arrival time: ' + data[i].arrivalTime +
                ' number: ' + data[i].trainNumber));
    }
    $('#showPassengersTableButton').show();
}

$(document).ready(function () {
    $('#showPassengersTableButton').click(function () {
        var trainId = $("#availableTrains").val();
        if (trainId === '') {
            $('.modal-title').html('ERROR');
            $('.modal-body').html('SELECT TRAIN');
            $('#myModal').modal('show');
            return;
        }
        $.ajax({
            type: "GET",
            url: path + "/admin/getPassengers",
            data: {
                trainId: trainId,
            },
            success: function (data) {
                fillPassengersTable(data);
            },
            error: function (data) {
                $('.modal-title').html('ERROR');
                $('.modal-body').html('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                    ' WE ARE WORKING ON THIS PROBLEM');
                $('#myModal').modal('show');
            }
        })
    })
});

function fillPassengersTable(data) {
    $('#passengerTable').remove();
    $('#PassengerTableContainer').append('<table class="table table-bordered" id="passengerTable">' +
        '<thead>' +
        '<th>Login</th>' +
        '<th>Name</th>' +
        '<th>Last number</th>' +
        '<th>Date of birth</th>' +
        '<th>Car.</th>' +
        '<th>Place</th>' +
        '<th>Start</th>' +
        '<th>End</th>' +
        '<th>Price</th>' +
        '</thead>' +
        '<tbody id="passengerTableBody"></tbody>' +
        '</table>');
    for(var i = 0; i < data.length; i++){
        $('#passengerTableBody').append('<tr><td>' + data[i].name + '</td><td>' + data[i].firstName + '</td>' +
            '<td>' + data[i].lastName + '</td><td>' + data[i].dateOfBirth + '</td><td>' + data[i].carriageNumber + '</td>' +
            '<td>' + data[i].placeNumber + '</td><td>' + data[i].startStationName + '</td>' +
            '<td>' + data[i].endStationName + '</td><td>' + data[i].price + '</td></tr>');
    }
}

function validateStation(station) {
    if(!stations.data.includes(station)){
        return false;
    }
    return true;
}

function fillStationList(index) {
    $('#selectedStation' + index).easyAutocomplete(stations);
}