$(document).ready(function () {
    $('#registerForm').on('submit', function f() {
        if(!$('#firstName').val()){
            $('.modal-title').html('ERROR');
            $('.modal-body').html('NAME FIELD SHOULD NOT BE EMPTY');
            $('#myModal').modal('show');
            return false;
        }
        if(!$('#lastName').val()){
            $('.modal-title').html('ERROR');
            $('.modal-body').html('SURNAME FIELD SHOULD NOT BE EMPTY');
            $('#myModal').modal('show');
            return false;
        }
        var datePattern = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$/;
        if(!datePattern.test($('#dateOfBirth').val())) {
            $('.modal-title').html('ERROR');
            $('.modal-body').html('INVALID DATE OF BIRTH FORMAT');
            $('#myModal').modal('show');
            return false;
        }
        if(!$('#login').val()){
            $('.modal-title').html('ERROR');
            $('.modal-body').html('LOGIN FIELD SHOULD NOT BE EMPTY');
            $('#myModal').modal('show');
            return false;
        }
        if(!$('#password').val()){
            $('.modal-title').html('ERROR');
            $('.modal-body').html('PASSWORD FIELD SHOULD NOT BE EMPTY');
            $('#myModal').modal('show');
            return false;
        }
    })
});
