var datePattern = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$/;
var dateTimePattern = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])([:])(0[0-9]|1[0-9]|2[0-3])([.])([0-5][0-9])$/;
var stations = {
    data:[],
    list: {
        match: {
            enabled:true
        }
    }
};

$(document).ready(function () {

    $.get('getStations', function (data) {
        for (var i = 0; i < data.length; i++) {
            stations.data.push(data[i]);
        }

    });
    $('.stationSelectAuto').easyAutocomplete(stations);
    $('#toCarriages').hide();
});

//timeTable functions

$(document).ready(function () {
    $('#showTimeTableButton').click(function () {
        var station = $("#selectStationForTimeTable").val();
        var date = $("#timeTableDate").val();
        if (!validateTimeTableInfo(date, station)) {
            return;
        }
        $.ajax({
            type: "GET",
            url: path + "/registered/showTimeTable",
            data: {
                station: station,
                date: date
            },
            success: function (data) {
                showTimeTable(data);
            },
            error: function (data) {
                $('.modal-title').html('ERROR');
                $('.modal-body').html('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                    ' WE ARE WORKING ON THIS PROBLEM');
                $('#myModal').modal('show');
            }
        })
    })
});

function validateTimeTableInfo(date, station) {
    if(!validateTimeTableDate(date)){
        return false;
    };
    if(!validateStation(station)){
        return false;
    }
    return true;

}

function validateTimeTableDate(date) {
    if (!datePattern.test(date)) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('INVALID DATE FORMAT');
        $('#myModal').modal('show');
        return false;
    }
    return true;
}

function showTimeTable(data) {
    $('#timeTable').empty();
    $('#timeTable').append('<thead>' +
        '<th>TRAIN NUMBER</th>' +
        '<th>ARRIVAL TIME</th>' +
        '<th>DEPARTURE TIME</th>' +
        '</thead>' +
        '<tbody id="timeTableBody"></tbody>' +
        '</table>');

    for (var i = 0; i < data.length; i++) {
        $('#timeTableBody').append('<tr><td>' + data[i].trainNumber + '</td><td>' + data[i].arrivalTime + '</td>' +
            '<td>' + data[i].departureTime + '</td></tr>');
    }
}

//routeFunctions

$(document).ready(function () {
    $('#showRoutes').click(function () {
        var startStation = $("#selectStartStation").val();
        var endStation = $("#selectEndStation").val();
        var startPeriod = $("#startTimeForRoute").val();
        var endPeriod = $("#endTimeForRoute").val();
        if (!validateStation(startStation)) {
            return;
        }
        if (!validateStation(endStation)) {
            return;
        }
        if (!validateStartTimeForRoute(startPeriod)) {
            return;
        }
        if (!validateEndTimeForRoute(endPeriod)) {
            return;
        }
        $.ajax({
            type: "GET",
            url: path + "/registered/showRoutes",
            data: {
                startStationName: startStation,
                endStationName: endStation,
                startPeriod: startPeriod,
                endPeriod: endPeriod
            },
            success: function (data) {
                showTrains(data);
            },
            error: function (data) {
                $('.modal-title').html('ERROR');
                $('.modal-body').html('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                    ' WE ARE WORKING ON THIS PROBLEM');
                $('#myModal').modal('show');
            }
        })
    })
});

function validateStartTimeForRoute(startPeriod) {
    if (!dateTimePattern.test(startPeriod)) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('START PERIOD: INVALID DATE TIME FORMAT');
        $('#myModal').modal('show');
        return false;
    }
    return true;
}

function validateEndTimeForRoute(endPeriod) {
    if (!dateTimePattern.test(endPeriod)) {
        $('.modal-title').html('ERROR');
        $('.modal-body').html('START PERIOD: INVALID DATE TIME FORMAT');
        $('#myModal').modal('show');
        return false;
    }
    return true;
}

function showTrains(data) {
    $('#trainTable').empty();
    if(data.length > 0) {
        $('#toCarriages').show();
    }
    $('#trainTable').append('<thead>' +
        '<th></th>' +
        '<th>TRAIN NUMBER</th>' +
        '<th>' + $("#selectStartStation").val() + ' DEPARTURE TIME</th>' +
        '<th>' + $("#selectEndStation").val() + ' ARRIVAL TIME' +
        '</thead>' +
        '<tbody id="trainTableBody"></tbody>' +
        '</table>');
    for (var i = 0; i < data.length; i++) {

        $('#trainTableBody').append('<tr>');
        if(data[i].canBuyTicket){
            $('#trainTableBody').append(
                '<td><input type="radio" class="form-check-input" name="trainRadio" value="' + data[i].trainId +'"/>')
        } else {
            $('#trainTableBody').append(
                '<td><input type="radio" class="form-check-input" name="trainRadio" disabled value="' + data[i].trainId +'"/>')
        }

        $('#trainTableBody').append(
            '<td>' + data[i].trainNumber + '</td>' +
            '<td>' + data[i].startTime + '</td>' +
            '<td>' + data[i].endTime + '</td>');
        $('#trainTableBody').append('</tr>');
    }
}


$(document).ready(function () {
    $('#toCarriages').click(function () {
        var selectedTrain = $('input[name = trainRadio]:checked').val();
        if( selectedTrain === undefined) {
            $('.modal-title').html('ERROR');
            $('.modal-body').html('SELECT TRAIN PLEASE');
            $('#myModal').modal('show');
            return;
        }
        $.ajax({
            type: "POST",
            url: path + "/registered/prepareCarriagesForm/",
            data: {
                selectedTrain: selectedTrain,
            },
            success: function (data) {
                location.href = path + "/registered/showCarriages/";
            },
            error: function (data) {
                $('.modal-title').html('ERROR');
                $('.modal-body').html('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                    ' WE ARE WORKING ON THIS PROBLEM');
                $('#myModal').modal('show');
            }
        });
    });
});

function validateStation(station) {
    if(!stations.data.includes(station)){
        $('.modal-title').html('ERROR');
        $('.modal-body').html(station + ': SUCH STATION DOES NOT EXISTS');
        $('#myModal').modal('show');
        return false;
    }
    return true;
}