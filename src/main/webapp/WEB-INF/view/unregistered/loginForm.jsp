
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE>
<html>
<head>
    <title>SBB Railroad</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/resources/main/bootstrap.min.css"/>">
</head>

<body>
<div class="row">
    <div class="col"></div>
    <div class="col text-center"><h4 style="color:#fff;background-color:#007bff">WELCOME TO SBB</h4></div>
    <div class="col"></div>
</div>
<div class="row">
    <div class="col"></div>
    <div class="col"><div class="alert-danger" align="center">${error}</div></div>
    <div class="col"></div>
</div>

<div class="row">
    <div class="col"></div>
    <div class="col">
            <form action="tryToLogin" method="post" id="loginForm">
                <div class="form-group text-center">
                    <label for="login">Login:</label>
                    <input type="text" required class="form-control text-center" id="login" placeholder="Enter your login" name="username">
                </div>
                <div class="form-group text-center">
                    <label for="pwd">Password:</label>
                    <input type="password" required class="form-control text-center" id="pwd" placeholder="Enter your password" name="password">
                </div>
                <button type="submit" class="btn btn-primary btn-block">LOG IN!</button>
            </form>
    </div>
    <div class="col"></div>
</div>

<div class="row">
    <div class="col"></div>
    <div class="col">
        <form:form method="GET"
                   action="${pageContext.request.contextPath}/unregistered/showRegistrationForm">
            <button type="submit" class="btn btn-primary btn-block">REGISTER</button>
        </form:form>
    </div>
    <div class="col"></div>
</div>

</body>
</html>