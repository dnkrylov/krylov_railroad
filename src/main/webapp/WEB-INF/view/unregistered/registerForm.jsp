<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <title>SBB Railroad</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/resources/main/bootstrap.min.css"/>">
    <script src="<c:url value="/resources/main/jquery.js" />"></script>
    <script src="<c:url value="/resources/pages/registerFormScripts.js"/>"></script>
    <script src="<c:url value="/resources/main/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/main/popper.min.js"/>"></script>
</head>
<body>
<jsp:include page="/resources/main/modalWindow.jsp"/>
<div class="row">
    <div class="col"></div>
    <div class="col text-center"><h4 style="color:#fff;background-color:#007bff">SBB REGISTRATION PAGE</h4></div>
    <div class="col"></div>
</div>

<div class="row">
    <div class="col"></div>
    <div class="col">
        <form:form method="POST"
                   action="${pageContext.request.contextPath}/unregistered/tryToRegister" modelAttribute="userDto"
                   id="registerForm">
            <div class="form-group text-center">
                <label for="firstName">NAME: </label>
                <form:input path="firstName" type="text" class="form-control text-center" id="firstName"
                            placeholder="enter your name"/>
                <div class="alert-danger align-content-center"><form:errors path="firstName"/></div>
            </div>
            <div class="form-group text-center">
                <label for="lastName">SURNAME:</label>
                <form:input path="lastName" type="text" class="form-control text-center" id="lastName"
                            placeholder="enter your surname"/>
                <div class="alert-danger align-content-center"><form:errors path="lastName"/></div>
            </div>
            <div class="form-group text-center">
                <label for="dateOfBirth">DATE OF BIRTH:</label>
                <form:input path="dateOfBirth" type="text" class="form-control text-center" id="dateOfBirth"
                            placeholder="dd.mm.yyyy formatted"/>
                <div class="alert-danger align-content-center"><form:errors path="dateOfBirth"/></div>
            </div>
            <div class="form-group text-center">
                <label for="login">LOGIN:</label>
                <form:input path="login" type="text" class="form-control text-center" id="login"
                            placeholder="enter your login"/>
                <div class="alert-danger align-content-center"><form:errors path="login"/></div>
            </div>
            <div class="form-group text-center">
                <label for="password">PASSWORD:</label>
                <form:input path="password" type="text" class="form-control text-center" id="password"
                            placeholder="enter your password"/>
                <div class="alert-danger align-content-center"><form:errors path="password"/></div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">REGISTER!</button>
        </form:form>
    </div>
    <div class="col"></div>
</div>

</div>

<div class="row">
    <div class="col"></div>
    <div class="col">
        <form:form method="GET"
                   action="${pageContext.request.contextPath}/unregistered/showLoginForm">
            <button type="submit" class="btn btn-danger btn-block">BACK TO LOGIN</button>
        </form:form>
    </div>
    <div class="col"></div>
</div>

</body>
</html>