<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <title>SBB Railroad</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/resources/main/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/pages/adminPageCss.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/main/easy-autocomplete.min.css"/>">
    <script src="<c:url value="/resources/main/jquery.js" />"></script>
    <script src="<c:url value="/resources/pages/adminPageScripts.js"/>"></script>
    <script src="<c:url value="/resources/main/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/main/popper.min.js"/>"></script>
    <script src="<c:url value="/resources/main/jquery.easy-autocomplete.min.js"/>"></script>
    <script type="text/javascript">var path = "${pageContext.request.contextPath}"</script>

</head>

<body>
<jsp:include page="/resources/main/modalWindow.jsp"/>
<div class="row">
    <div class="col text-center">
        <h4 style="color:#fff;background-color:#007bff">SBB ADMIN PAGE</h4>
    </div>
</div>
<div class="row">
    <div class="col text-center">
        <form method="GET"
              action="${pageContext.request.contextPath}/unregistered/tryToLogout">
            <button type="submit" class="button btn-block btn-danger">Logout</button>
        </form>
    </div>
    <div class="col text-center">
        <button class="button btn-primary btn-block" id="addNewStation">Create station</button>
    </div>
    <div class="col text-center">
        <button class="button btn-primary btn-block" id="addTrain">Create train</button>
    </div>
    <div class="col text-center">
        <button class="button btn-primary btn-block" id="showPassengers">Show passengers</button>
    </div>
    <div class="col text-center">
        <form method="GET" cssClass="text-right"
              action="${pageContext.request.contextPath}/registered/showHomePageForm">
            <button type="submit" class="button btn-block btn-success">To homepage</button>
        </form>
    </div>
</div>
<%----%>
<form class="adminForm" id="trainCreationForm">
    <div class="row">
        <div class="col"></div>
        <div class="col text-center">
            <div class="form-group">
                <label for="trainNumber">INPUT TRAIN NUMBER</label>
                <input type="text" class="form-control text-center" id="trainNumber"
                       placeholder="enter train name"/>
            </div>
        </div>
        <div class="col"></div>
    </div>
    <div class="row">
        <div class="col text-center">
            <table class="table table-bordered trainTable" id="stationTableForm">
                <thead>
                <tr>
                    <th>
                        SELECT STATION
                    </th>
                    <th>
                        ARRIVING TIME
                    </th>
                    <th>
                        DEPARTURE TIME
                    </th>
                    <th>
                        ADDITIONAL PRICE (BUSINESS)
                    </th>
                    <th>
                        ADDITIONAL PRICE (COMFORT)
                    </th>
                    <th>
                        ADDITIONAL PRICE (ECONOMY)
                    </th>
                </tr>
                </thead>
                <tbody id="stationTable">

                </tbody>
            </table>
            <button type="button" class="button btn-success" id="addStation">ADD STATION</button>
            <button type="button" class="button btn-danger" id="removeStation">REMOVE STATION</button>
        </div>
    </div>
    <div class="row">
        <div class="col text-center">
            <table class="table table-bordered trainTable" id="carriageTableForm">
                <thead>
                <tr>
                    <th>
                        CARRIAGE NUMBER
                    </th>
                    <th>
                        CARRIAGE TYPE
                    </th>
                    <th>
                        CARRIAGE CAPACITY
                    </th>
                </tr>
                </thead>
                <tbody id="carriageTable">

                </tbody>
            </table>
            <button type="button" class="button btn-success" id="addCarriage">ADD CARRIAGE</button>
            <button type="button" class="button btn-danger" id="removeCarriage">REMOVE CARRIAGE</button>
        </div>
    </div>
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <div class="container">
                <button type="button" class="button btn-success btn-block" id="submitTrain">
                    SUBMIT TRAIN
                </button>
            </div>
        </div>
        <div class="col"></div>
    </div>
</form>
<%----%>


<form class="adminForm" id="passengerShowForm">
    <div class="row">
        <div class="col text-center">
            <div class="form-group">
                <label for="newStationName">INPUT DATE: </label>
                <input type="text" class="form-control text-center" id="dateForTimeTable"
                       placeholder="dd.mm.yyyy format"/>
            </div>
        </div>
        <div class="col text-center">
            <div class="form-group">
                <label for="stationForTimeTable">SELECT STATION: </label>
                <input class="form-control stationSelectAuto text-center" autocomplete="off" id="stationForTimeTable"
                       placeholder="start typing"/>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col"></div>
        <div class="col text-center">
            <button type="button" class="button btn-success" id="getTrains">FIND
                TRAINS
            </button>
        </div>
        <div class="col"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col text-center" id="trainSelection">
            <button type="button" class="button btn-success" id="showPassengersTableButton"
                    style="margin-top: 10px; margin-bottom: 10px">
                SHOW PASSENGERS
            </button>
        </div>
        <div class="col-sm-2">
        </div>
    </div>
    <div class="row">
        <div class="col text-center">
            <div class="container" id="PassengerTableContainer">
            </div>
        </div>
    </div>
</form>


<form class="adminForm" id="stationAdditionForm">
    <div class="row">
        <div class="col"></div>
        <div class="col text-center">
            <div class="form-group">
                <label for="newStationName">INPUT NAME FOR NEW STATION: </label>
                <input type="text" class="form-control text-center" id="newStationName"
                       placeholder="station name"/>
            </div>
        </div>
        <div class="col"></div>
    </div>
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <button type="button" class="button btn-success btn-block" id="submitStation">
                SUBMIT STATION
            </button>
            <br/>
        </div>
        <div class="col"></div>
    </div>
</form>

</body>
</html>
