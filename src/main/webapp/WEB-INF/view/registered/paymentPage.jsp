<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <title>SBB Railroad</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/resources/main/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/pages/paymentPageCss.css"/>">
    <script src="<c:url value="/resources/main/jquery.js" />"></script>
    <script src="<c:url value="/resources/pages/paymentPageScripts.js"/>"></script>
    <script src="<c:url value="/resources/main/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/main/popper.min.js"/>"></script>
    <script type="text/javascript">var path = "${pageContext.request.contextPath}"</script>
</head>
<body>
<jsp:include page="/resources/main/modalWindow.jsp"/>
<div class="row">
    <div class="col">
        <h4 class="simpleInfoField">YOUR TICKET:</h4>
        <div class="form-group text-center">
            <table class="table table-bordered">
                <tr>
                    <td>START STATION:</td>
                    <td>${routeInfo.startStationName}</td>
                </tr>
                <tr>
                    <td>DEPARTING AT:</td>
                    <td>${routeInfo.departureTime}</td>
                </tr>
                <tr>
                    <td>END STATION:</td>
                    <td>${routeInfo.endStationName}</td>
                </tr>
                <tr>
                    <td>ARRIVING AT:</td>
                    <td>${routeInfo.arrivingTime}</td>
                </tr>
                <tr>
                    <td>TRAIN:</td>
                    <td>${routeInfo.trainNumber}</td>
                </tr>
                <tr>
                    <td>CARRIAGE:</td>
                    <td>${routeInfo.carriageNumber}, ${routeInfo.carriageType}</td>
                </tr>
                <tr>
                    <td>PLACE:</td>
                    <td>${routeInfo.placeNumber}</td>
                </tr>
                <tr>
                    <td>PASSENGER:</td>
                    <td>${routeInfo.userFirstName} ${routeInfo.userLastName} ${routeInfo.userDateOfBirth}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col text-center" id="paymentInfoCol">
            <form action="${pageContext.request.contextPath}/registered/tryToBuyTicket/" method="post"
                  modelAttribute="paymentInfo">
                <h4 class="simpleInfoField">INPUT PAYMENT INFO</h4>
                <h4>TICKET PRICE: ${routeInfo.ticketPrice}</h4>
                <div class="form-group text-center">
                    <label for="cardNumber">credit card number: </label>
                    <input type="text" class="form-control text-center" id="cardNumber"
                           placeholder="XXXXXXXXXXXXXXXX"/>
                </div>
                <div class="form-group text-center">
                    <label for="cardExp">credit card expiration: </label>
                    <input type="text" class="form-control text-center" id="cardExp"
                           placeholder="mm/yy"/>
                </div>
                <div class="form-group text-center">
                    <label for="cardCVV">CVV: </label>
                    <input type="text" class="form-control text-center" id="cardCVV"
                           placeholder="XXX"/>
                </div>
                <button type="button" class="button btn-success" id="buyTicket">BUY TICKET!</button>
            </form>
        </div>
</div>
<div class="row">
    <div class="col text-center">
        <form:form method="GET" cssClass="text-left"
                   action="${pageContext.request.contextPath}/registered/showCarriages">
            <button type="submit" class="button btn-danger btn-block text-center" id="changePlaceButton">CHANGE PLACE</button>
        </form:form>
    </div>
    <div class="col text-center">
        <form:form method="GET" cssClass="text-center"
                   action="${pageContext.request.contextPath}/registered/showHomePageForm">
            <button type="submit" class="button btn-danger btn-block text-center">RETURN TO HOMEPAGE</button>
        </form:form>
    </div>
</div>
</body>
</html>
