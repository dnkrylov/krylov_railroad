<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <head>
        <title>SBB Railroad</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<c:url value="/resources/main/bootstrap.min.css"/>">
        <link rel="stylesheet" href="<c:url value="/resources/pages/ordinaryUserCss.css"/>">
        <link rel="stylesheet" href="<c:url value="/resources/main/easy-autocomplete.min.css"/>">
        <script src="<c:url value="/resources/main/jquery.js" />"></script>
        <script src="<c:url value="/resources/pages/ordinaryUserPageScripts.js"/>"></script>
        <script src="<c:url value="/resources/main/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resources/main/popper.min.js"/>"></script>
        <script src="<c:url value="/resources/main/jquery.easy-autocomplete.min.js"/>"></script>
        <script type="text/javascript">var path = "${pageContext.request.contextPath}"</script>
    </head>
</head>
<body>
<jsp:include page="/resources/main/modalWindow.jsp"/>
<div class="row">

    <div class="col text-center">
        <h4 style="color:#fff;background-color:#007bff">
            HERE YOU CAN CHECK TIMETABLE OR FIND TRAINS
        </h4>
    </div>
</div>
<div class="row">
    <div class="col text-center">Show timetable:</div>
    <div class="col text-center">Find route:</div>
</div>
<div class="row text-center">
    <div class="col">
        <form class="userForm" id="timeTableForm">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="selectStationForTimeTable">SELECT STATION: </label>
                        <input class="form-control stationSelectAuto text-center" autocomplete="off" id="selectStationForTimeTable"
                               placeholder="start typing"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="timeTableDate">INPUT DATE</label>
                        <input type="text" class="form-control text-center" id="timeTableDate"
                               placeholder="dd.mm.yyyy format"/>
                    </div>
                    <button type="button" class="button btn-primary btn-block" id="showTimeTableButton">
                        SHOW TIMETABLE
                    </button>
                </div>
            </div>
        </form>
    </div>

    <div class="col">
        <form class="userForm" id="routeTableForm">
            <div class="row">
                <div class="col text-center">
                    <div class="form-group">
                        <label for="selectStartStation">SELECT START STATION: </label>
                        <input class="form-control stationSelectAuto text-center" autocomplete="off" id="selectStartStation"
                               placeholder="start typing"/>
                    </div>
                </div>
                <div class="col text-center">
                    <div class="form-group">
                        <label for="selectEndStation">SELECT STATION: </label>
                        <input class="form-control stationSelectAuto text-center" autocomplete="off" id="selectEndStation"
                               placeholder="start typing"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="startTimeForRoute">STARTING BETWEEN: </label>
                        <input type="text" class="form-control text-center" id="startTimeForRoute"
                               placeholder="dd.mm.yyyy:hh.mm format"/>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="endTimeForRoute">AND: </label>
                        <input type="text" class="form-control text-center" id="endTimeForRoute"
                               placeholder="dd.mm.yyyy:hh.mm format"/>
                    </div>
                </div>
            </div>
            <button type="button" class="button btn-primary btn-block" id="showRoutes">FIND TRAINS!</button>
        </form>
    </div>
</div>
<div class="row text-center">
    <div class="col text-center">
        <div class="container" id="timeTableContainer">
            <table class="table table-hover" id="timeTable">

            </table>
        </div>
    </div>
    <div class="col text-center">
        <div class="container" id="routeContainer">
            <table class="table table-hover" id="trainTable">

            </table>
            <button type="button" id="toCarriages" class="button btn-success">CHOOSE PLACE</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col text-left">
        <form:form method="GET"
                   action="${pageContext.request.contextPath}/unregistered/tryToLogout">
            <button type="submit" class="button btn-danger">LOGOUT</button>
        </form:form>
    </div>
    <div class="col"></div>
    <div class="col"></div>
    <div class="col"></div>
    <div class="col"></div>
    <div class="col"></div>
    <div class="col text-right">
        <security:authorize access="hasRole('ROLE_ADMIN')">
            <form:form method="GET"
                       action="${pageContext.request.contextPath}/admin/showAdminPage">
                <button type="submit" class="button btn-success">to admin page</button>
            </form:form>
        </security:authorize>
    </div>
</div>
</body>
</html>