<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <head>
        <title>SBB Railroad</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<c:url value="/resources/main/bootstrap.min.css"/>">
        <link rel="stylesheet" href="<c:url value="/resources/pages/placeSelectionCss.css"/>">
    </head>
</head>
<body>
<div class="row">
    <div class="col text-center">
        <div class="row text-center">
            <div class="col text-center">
                <h4 style="color:#fff;background-color:#007bff">CHOOSE CARRIAGE AND PLACE</h4>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">CARRIAGE NUMBER</div>
            <div class="col text-center">CARRIAGE TYPE</div>
            <div class="col text-center">TICKET PRICE</div>
            <div class="col text-center">FREE PLACES</div>
            <div class="col text-center">SELECT PLACE</div>
            <div class="col text-center">SUBMIT</div>
        </div>
        <c:forEach var="carriage" items="${carriages}">

            <form method="post" action="${pageContext.request.contextPath}/registered/goToPaymentPage/">
                <div class="row carriageRow">
                    <div class="col text-center">
                        <input type="hidden" name="carriageId" value="${carriage.carriageId}">
                            ${carriage.number}
                    </div>
                    <div class="col text-center">
                            ${carriage.type}
                        <input type="hidden" name="carriageType" value="${carriage.type}">
                    </div>
                    <div class="col text-center">
                            ${carriage.ticketPrice}
                    </div>
                    <div class="col text-center">
                            ${carriage.availablePlaces}/${carriage.capacity}
                    </div>
                    <div class="col text-center">
                        <select class="form-control" name="placeNumber">
                            <c:forEach var="place" items="${carriage.freePlaceNumbers}">
                                <option>${place}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col text-center">
                        <button type="submit" class="button btn-success">SELECT THIS</button>
                    </div>
                </div>
            </form>
        </c:forEach>
    </div>
</div>
<div class="row">
    <div class="col text-left">
        <form:form method="GET"
                   action="${pageContext.request.contextPath}/registered/showHomePageForm">
            <button type="submit" class="btn btn-danger" id="homePageButton">to homepage</button>
        </form:form>
    </div>
</div>
</body>
</html>